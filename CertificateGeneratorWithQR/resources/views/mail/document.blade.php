@component('mail::message')

From: {{ $details['fromEmail'] }}

Message: {{ $details['body'] }}

Training : {{ $details['training'] }}

Name : {{ $details['name'] }}

Date : {{ $details['day'] }}

Certificate ID: {{ $details['id'] }}

@component('mail::button', ['url' => $details['url']])
Download your Certificate Here
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent