<x-app-layout>

</x-app-layout>


<!DOCTYPE html>
<html lang="en">
  <head>

    @include("admin.admincss")

  </head>
  <style>
table {
  border-collapse: collapse;
  width: 100%;
}

th, td {
  text-align: center;
  padding: 8px;
  border: 1px solid #20B2AA;
}


th {
  background-color: #20B2AA;
  color: white;
}

</style>
  <body>

  	<div class="container-scroller">

    @include("admin.navbar")

    <div style="position: relative; top: 5px;">
    	
    	<table >
    		<tr>
    			<th style="padding: 30px;">Name</th>
    			<th style="padding: 30px;">Email Address</th>
    			<th style="padding: 30px;">Phone Number</th>
    			<th style="padding: 30px;">Qty</th>
    			<th style="padding: 30px;">Date</th>
    			<th style="padding: 30px;">Time</th>
    			<th style="padding: 30px;">Message</th>
    	    </tr>

    			@foreach($data as $data)


    		<tr align="center" bgcolor="#f2f2f2">
    			<td style="color:black">{{$data->name}}</td>
    			<td style="color:black">{{$data->email}}</td>
    			<td style="color:black">{{$data->phone}}</td>
    			<td style="color:black">{{$data->guest}}</td>
    			<td style="color:black">{{$data->date}}</td>
    			<td style="color:black">{{$data->time}}</td>
    			<td style="color:black">{{$data->message}}</td>
            </tr>
    			   @endforeach

    	</table>
    </div>
</div>

    @include("admin.adminscript")

  </body>
</html>