<x-app-layout>

</x-app-layout>


<!DOCTYPE html>
<html lang="en">
  <head>

    @include("admin.admincss")

  </head>
  <style>
table {
  border-collapse: collapse;
  width: 100%;
}

th, td {
  text-align: center;
  padding: 8px;
  border: 1px solid #04AA6D;
}


th {
  background-color: #04AA6D;
  color: white;
}
input[type=submit] {
  width: 10%;
  height: 35px;
  background-color: #04AA6D;
  color: white;
  padding: 5px 5px;
  margin: 8px 0;
  border: none;
  border-radius: 4px;
  cursor: pointer;
  }

</style>
  <body>
  	<div class="container-scroller">

    @include("admin.navbar")

    <div class="container" style="position: relative; top: 5px; margin-left: 30px;">

    <form action="{{url('/search')}}" method="get">

    	@csrf
      <div class="col-12">
    	<input type="text" name="search" placeholder="Enter search" style="color: blue">
    	<input type="submit" value="Search" class="btn btn-success">
      </div>

    </form>

    <div style="position: relative; top: 5px;">
    	
    	<table >
    		<tr>
    			<th style="padding: 30px;">Name</th>
    			<th style="padding: 30px;">Phone</th>
    			<th style="padding: 30px;">Address</th>
    			<th style="padding: 30px;">Foodname</th>
    			<th style="padding: 30px;">Price</th>
    			<th style="padding: 30px;">Quantity</th>
    			<th style="padding: 30px;">Total Price</th>
    	    </tr>

    			@foreach($data as $data)


    		<tr align="center" bgcolor="#f2f2f2">
    			<td style="color:black">{{$data->name}}</td>
    			<td style="color:black">{{$data->phone}}</td>
    			<td style="color:black">{{$data->address}}</td>
    			<td style="color:black">{{$data->foodname}}</td>
    			<td style="color:black">{{$data->price}}</td>
    			<td style="color:black">{{$data->quantity}}</td>
    			<td style="color:black">{{$data->price * $data->quantity}}</td>
            </tr>
    			   @endforeach

    	</table>
    </div>
	
	</div>

    @include("admin.adminscript")

  </body>
</html>