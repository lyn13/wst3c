<x-app-layout>

</x-app-layout>


<!DOCTYPE html>
<html lang="en">
  <head>

  	<base href="/public">

    @include("admin.admincss")

  </head>
   <style>
  label {
  color: black;
  }

  input[type=text] {
  width: 100%;
  padding: 12px 20px;
  margin: 4px 0;
  display: inline-block;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
  height: 15px;
  color: black;
  }

  input[type=number] {
  width: 100%;
  padding: 12px 20px;
  margin: 4px 0;
  display: inline-block;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
  height: 15px;
  color: black;
  }

  input[type=submit] {
  width: 40%;
  height: 35px;
  background-color: #04AA6D;
  color: white;
  padding: 5px 5px;
  margin: 8px 0;
  border: none;
  border-radius: 4px;
  cursor: pointer;
  margin-left: 90px;
  }

  .container {
  border-radius: 5px;
  background-color: #f2f2f2;
  padding: 20px;
  height: 515px;
  width: 300px;
}
  </style>
  <body>

  	<div class="container-scroller">
    @include("admin.navbar")
    
      <div class="container" style="margin-top: 5px; margin-right: 400px;" >
    	<form action="{{url('/update',$data->id)}}" method="post" enctype="multipart/form-data">

            @csrf

    		  <div class="row">
          <div class="col-10">
    			<label for="title">Title:</label>
          </div>
          <div class="col-12">
    			<input type="text" name="title" value="{{$data->title}}" required>
    		  </div>
        </div>

    		<div class="row">
          <div class="col-10">
    			<label for="price">Price:</label>
          </div>
          <div class="col-12">
    			<input  type="number" name=" price" value="{{$data->price}}" required>
          </div>
    		</div>

    		<div class="row">
          <div class="col-10">
    			<label for="description">Description:</label>
          </div>
          <div class="col-12">
    			<input type="text" name="description" value="{{$data->description}}" required>
          </div>
    		</div>

    		<div class="row">
          <div class="col-10">
    			<label for="image">Old Image</label>
          </div>
          <div class="col-12">
    			<center><img height="120" width="120" src="/foodimage/{{$data->image}}"></center>
          </div>
    		</div>

    		<div class="row">
          <div class="col-10">
    			<label for="image">New Image</label>
          </div>
          <div class="col-12">
    			<input style="color: black" type="file" name="image" required>
          </div>
    		</div>


    		<div class="row">
    			 <input type="submit" value="Save">
    		</div>
    	</form>
    </div>
    </div>

            <div>

	</div>
    @include("admin.adminscript")

  </body>
</html>