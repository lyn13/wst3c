<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Order Details</title>
 
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<script>
    
</script>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-success shadow-sm">
            <div class="container">
                <a class="navbar-brand text-white active" href="{{ url('/') }}">
                    {{ config('', 'Item') }} 
                </a>
                <a class="navbar-brand text-white" href="{{ url('/customer') }}">
                    {{ config('', 'Customer') }}
                </a>
                <a class="navbar-brand text-white" href="{{ url('/order') }}">
                    {{ config('', 'Order') }}
                </a>
                <a class="navbar-brand text-white" href="{{ url('/orderdetails') }}">
                    {{ config('', 'Orderd Details') }}
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse te" id="navbarSupportedContent" >
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav me-auto ">
                        <!-- Authentication Links -->
                    </ul>
                </div>
            </div>
        </nav>

    <div class="container d-flex  flex-column">
    <div class="col-md-4 col-xs-10 col-sm-6 col-lg-4">
                          
  <h1><b>Order Details</b></h1><br>
  <h2 class="masthead-heading  mb-0 "><b>Transaction No:</b> <?php echo $orTN; ?></h2>
  <h2 class="masthead-heading  mb-0 "><b>No:</b> <?php echo $orNo; ?></h2> 
  <h2 class="masthead-heading  mb-0 "><b>ID:</b> <?php echo $orID; ?></h2> 
  <h2 class="masthead-heading  mb-0 "><b>Name:</b> <?php echo $orNa; ?></h2> 
  <h2 class="masthead-heading  mb-0 "><b>Price:</b> <?php echo $orPri; ?></h2> 
  <h2 class="masthead-heading  mb-0 "><b>Quantity:</b> <?php echo $orQt; ?></h2> 
</div>
</body>
</html>
