<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/*Route::view('/', 'home'); 
Route::view('/register', 'register');
Route::view('/login', 'login');
Route::view('/about', 'about');
Route::view('/resume', 'resume');
Route::get('/contact', 'App\Http\Controllers\ContactFormController@create'); 
Route::post('/contact', 'App\Http\Controllers\ContactFormController@store'); 

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');*/



/*Route::view('/', 'item'); 
Route::view('/order', 'order');
Route::view('/customer', 'customer');
Route::view('/orderdetails', 'orderdetails');
Route::view('/item', 'item');
*/
/*Route::get('/', function () {
    return 'Hello, World!';})*/;

Route::redirect('/here', '/there');

//Route::view('/welcome', '/welcome');

Route::view('/welcome', 'welcome',['name'=>'Taylor']);

/*$url= route('welcome);   

Route::get('/',function()  
{  
return view('studentnew');  
});*/

Route::get('/student',function()  
{  
return view('student');  
});  
Route::get('student/details',function()  
{  
$url=route('student.details');  
//$url ="<a href='#'>testsample</a>";
return $url;  
})->name('student.details');  


Route::get('/about', function()  
{  
  return "This is a about us page";   
});  


Route::get('/post/{id}', function($id)  
{  
	return "id number is : ". $id;   
} );
Route::get('customer/{Cid?}/{Cname?}/{Caddress?}', function($Cid = '1',$Cname = 'Edilyn De Guzman',$Caddress= 'Turac San Carlos ')
{
    return 'customer'.$Cid.$Cname.$Caddress;
});

Route::get('item/{Inum?}/{Iname?}/{Iprice?}', function($Inum = '1',$Iname = 'Stitch Pillow',$Iprice= '500 ')
{
    return 'item'.$Inum.$Iname.$Iprice;
});

Route::get('order/{Oid?}/{Oname?}/{Onum?}/{Odate?}', function($Oid = '000001',$Oname = 'Stitch Pillow',$Onum= '1 ', $Odate = 'April 20, 2022')
{
    return 'order'.$Oid.$Oname.$Onum.$Odate;
});

Route::get('orderlist/{Otn?}/{Ono?}/{Oid?}/{Oname?}/{Oprice?}/{Oqty?}', function($Otn = '123456789',$Ono = '1',$Oid= '000001', $Oname = 'Stitch Pillow', $Oprice = '500', $Oqty= '1')
{
    return 'orderlist'.$Otn.$Ono.$Oid.$Oname.$Oprice.$Oqty;
});


