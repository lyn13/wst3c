<?php

namespace App\Http\Controllers;

use App\Mail\ContactFormMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactFormController extends Controller
{
    public function create()
    {
        return view('contact.create');
    }

    public function store()
    {
        $data = request()->validate([
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required',
        ]);
       
        Mail::to('deguzmanedilyn13@gmail.com')->send(new ContactFormMail($data));
        return back()->with('contact', 'We have received your message and would like to thank you for writing to us.');
    }
    
}
