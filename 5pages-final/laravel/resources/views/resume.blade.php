@extends('layouts.app')
@section('content')
 <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
<div class="container bootstrap snippets bootdeys">
    <div class="row" id="user-profile">
        <div class="col-lg-3 col-md-4 col-sm-4">
            <div class="main-box clearfix align-items-center ">
                <center><img src="https://scontent.fmnl3-1.fna.fbcdn.net/v/t39.30808-6/219875448_1941150512718200_6276753955770293138_n.jpg?_nc_cat=108&ccb=1-5&_nc_sid=174925&_nc_eui2=AeGjLwwNpci_Q1cpSX0LpLC3fZYFEWt2FOd9lgURa3YU5zjcXQLAgp_oKOuGdx4PbCM9kwFNkhTzDpe90CivE8R3&_nc_ohc=PtkVrnlQQqAAX_tcY4B&tn=5VWzK6HzSt2MpkIz&_nc_zt=23&_nc_ht=scontent.fmnl3-1.fna&oh=00_AT8GClAxuf614oHfvfY4m_H7AddTLPI0JHoeMu1FFVlL8Q&oe=6238E95C" alt="" class="rounded-circle" width="175" ></center>
                <div class="profile-label">
                </div>
                    <div class="mt-3">
                      <center><h4><b>EDILYN R. DE GUZMAN</b></h4></center>
                      <center><p class="text-secondary mb-1">College Student</p></center>
                      <center><p class="text-muted font-size-sm">Turac, Malay Pogus, SCCP</p></center>
               
              <div class="card-box">
                <h4 class="m-t-0 m-b-20 header-title"><center><b>Coding Language</b></center></h4>
                <div class="p-b-10">
                    <p>CSS</p>
                    <div class="progress progress-sm">
                        <div class="progress-bar progress-bar bg-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                        </div>
                    </div>
                    <p>JAVASCRIPT</p>
                    <div class="progress progress-sm">
                        <div class="progress-bar progress-bar bg-danger" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                        </div>
                    </div>
                    <p>BOOTSTRAP</p>
                    <div class="progress progress-sm">
                        <div class="progress-bar progress-bar bg-info" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
                        </div>
                    </div>
                    <p>SQL</p>
                    <div class="progress progress-sm m-b-0">
                        <div class="progress-bar progress-bar bg-success" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%">
                        </div>
                    </div>
                    <p>HTML</p>
                    <div class="progress progress-sm m-b-0">
                        <div class="progress-bar progress-bar bg-primary" role="progressbar" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100" style="width: 65%">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        </div>
        
         <!-- end col -->

        <div class="col-lg-9 col-md-8 col-sm-8">
            <div class="main-box clearfix">
                <div class="profile-header">
                    <h3><span>PERSONAL INFORMATION</span></h3>
                    

                <div class="row profile-user-info">
                    <div class="col-sm-8">
                        <div class="profile-user-details clearfix">
                            <div class="profile-user-details-label">
                                Name
                            </div>
                            <div class="profile-user-details-value">
                                Edilyn R. De Guzman
                            </div>
                        </div>
                        <div class="profile-user-details clearfix">
                            <div class="profile-user-details-label">
                                Citizenship
                            </div>
                            <div class="profile-user-details-value">
                                Filipino
                            </div>
                        </div>
                        <div class="profile-user-details clearfix">
                            <div class="profile-user-details-label">
                                Birthday
                            </div>
                            <div class="profile-user-details-value">
                                June 13,1999
                            </div>
                        </div>
                        <div class="profile-user-details clearfix">
                            <div class="profile-user-details-label">
                                Age
                            </div>
                            <div class="profile-user-details-value">
                                22 years old
                            </div>
                        </div>
                        <div class="profile-user-details clearfix">
                            <div class="profile-user-details-label">
                                Civil Status
                            </div>
                            <div class="profile-user-details-value">
                                Single
                            </div>
                        </div>
                        <div class="profile-user-details clearfix">
                            <div class="profile-user-details-label">
                                Religion
                            </div>
                            <div class="profile-user-details-value">
                                Roman Catholic
                            </div>
                        </div>
                        <div class="profile-user-details clearfix">
                            <div class="profile-user-details-label">
                                Cell No.
                            </div>
                            <div class="profile-user-details-value">
                                0966-413-6146
                            </div>
                        </div>
                        <div class="profile-user-details clearfix">
                            <div class="profile-user-details-label">
                                Email
                            </div>
                            <div class="profile-user-details-value">
                                edilyndeguzman23@yahoo.com
                            </div>
                        </div>
                        <div class="profile-user-details clearfix">
                            <div class="profile-user-details-label">
                                Address
                            </div>
                            <div class="profile-user-details-value">
                                Turac Malay Pogus,SCCP
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 profile-social">
                        <ul class="fa-ul">
                            <li><i class="fa-li fa fa-twitter-square"></i><a href="#">@dgedilyn</a></li>
                            <li><i class="fa-li fa fa-linkedin-square"></i><a href="#">Edilyn De Guzman </a></li>
                            <li><i class="fa-li fa fa-facebook-square"></i><a href="#">Edilyn Rilloma De Guzman </a></li>
                            <li><i class="fa-li fa fa-skype"></i><a href="#">stitch_13</a></li>
                            <li><i class="fa-li fa fa-instagram"></i><a href="#">edlynrillomadgz</a></li>
                        </ul>
                    </div>
                </div>
              </div>
</div>
     <div class="col-lg-12">
            <div class="main-box clearfix">
                <div class="profile-header">
                    <h3><span>EDUCATIONAL ATTAINMENT</span></h3>

                    <ul class="list-unstyled timeline-sm">
                        <li class="timeline-sm-item">
                            <span class="timeline-sm-date">2006 - 13</span>
                            <h5 class="mt-0 mb-1">Elementary</h5>
                            <h5 class="mt-0 mb-1"><b>School:</b></h5>
                            <p class="text-muted mt-2">Aquikino Banaag Elementart School</p>
                            <h5 class="mt-0 mb-1"><b>Address:</b></h5>
                            <p class="text-muted mt-2">Banaag Street, Basista Pangasinan</p>
                        </li>
                        <li class="timeline-sm-item">
                            <span class="timeline-sm-date">2012 - 18</span>
                            <h5 class="mt-0 mb-1">Secondary</h5>
                            <h5 class="mt-0 mb-1"><b>School:</b></h5>
                            <p class="text-muted mt-2">Turac National High School</p>
                            <h5 class="mt-0 mb-1"><b>Address:</b></h5>
                            <p class="text-muted mt-2">Brgy. Turac San Carlos City, Pangasinan</p>
                        </li>
                        <li class="timeline-sm-item">
                            <span class="timeline-sm-date">2012 - 18</span>
                            <h5 class="mt-0 mb-1">Tertiary</h5>
                            <h5 class="mt-0 mb-1"><b>School:</b></h5>
                            <p class="text-muted mt-2">Pangasinan State University of Urdaneta Campus</p>
                            <h5 class="mt-0 mb-1"><b>Address:</b></h5>
                            <p class="text-muted mt-2">Urdaneta City Pangasinan </p>
                        </li>
                    </ul>
                  </div>
                </div>

                   <div class="col-lg-12">
            <div class="main-box clearfix">
                <div class="profile-header">
                    <h3><span>STUDENT SKILLS</span></h3>


                    <div class="sm-no-margin">
                        <div class="progress-text">
                            <div class="row">
                                <div class="col-7">Positive Behaviors</div>
                                <div class="col-5 text-right">40%</div>
                            </div>
                        </div>
                        <div class="custom-progress progress">
                            <div role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:40%" class="animated custom-bar progress-bar slideInLeft bg-warning"></div>
                        </div>
                        <div class="progress-text">
                            <div class="row">
                                <div class="col-7">Teamworking Abilities</div>
                                <div class="col-5 text-right">50%</div>
                            </div>
                        </div>
                        <div class="custom-progress progress">
                            <div role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:50%" class="animated custom-bar progress-bar slideInLeft bg-danger"></div>
                        </div>
                        <div class="progress-text">
                            <div class="row">
                                <div class="col-7">Time Management </div>
                                <div class="col-5 text-right">60%</div>
                            </div>
                        </div>
                        <div class="custom-progress progress">
                            <div role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:60%" class="animated custom-bar progress-bar slideInLeft bg-info"></div>
                        </div>
                        <div class="progress-text">
                            <div class="row">
                                <div class="col-7">Excellent Communication</div>
                                <div class="col-5 text-right">50%</div>
                            </div>
                        </div>
                        <div class="custom-progress progress">
                            <div role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:50%" class="animated custom-bar progress-bar slideInLeft bg-success"></div>
                        </div>
                    </div>
                  </div>
                </div>
                 <div class="col-lg-12">
            <div class="main-box clearfix">
                <div class="profile-header">
                    <center><h3><span>GALLERY</span></h3></center>
                 <div class="tab-pane active" id="profile">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="gal-detail thumb">
                                        <a href="#" class="image-popup" title="Screenshot-2">
                                            <img src="https://scontent.fmnl3-1.fna.fbcdn.net/v/t1.15752-9/275398569_3063629550621724_4293258664496249518_n.jpg?_nc_cat=110&ccb=1-5&_nc_sid=ae9488&_nc_eui2=AeFffvc02XJXlZbPoJ4ZS6AsyNqesdC8jx3I2p6x0LyPHWQZ3-IfJ3ps8oBP1wBJcat8zgxDgo-95Nv_iCZyc9Ns&_nc_ohc=pBwUCyYuT70AX_ygo7F&_nc_ht=scontent.fmnl3-1.fna&oh=03_AVK5SYJCKugbNUKVy0w7w7yeCk_jBETBNwIIxTjm9WGDlQ&oe=625DC315" class="thumb-img" alt="work-thumbnail" width="250">
                                        </a>
                                        <h4 class="text-center">Family</h4>
                                        <div class="ga-border"></div>
                                        <p class="text-muted text-center"><small><b>"Family is not an important thing. It's everything"</b></small></p>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="gal-detail thumb">
                                        <a href="#" class="image-popup" title="Screenshot-2">
                                            <img src="https://scontent.fmnl3-3.fna.fbcdn.net/v/t1.15752-9/275551529_1397593444020791_2037867511903863121_n.jpg?_nc_cat=111&ccb=1-5&_nc_sid=ae9488&_nc_eui2=AeGxfD7drZ8eFogwjWRRzlE3U_2ZXqlBmtRT_ZleqUGa1KqJeJVToZq42usIYEZbwSf9aPTuLOK5-LBJrBswZttL&_nc_ohc=xx8abJyRKyQAX8ejVs3&_nc_ht=scontent.fmnl3-3.fna&oh=03_AVKh7KppNQPpZfdllDQl1mZvKWjDamLbXsMlHs4yTHI1qQ&oe=625E49CE" class="thumb-img" alt="work-thumbnail" width="250">
                                        </a>
                                        <h4 class="text-center">Friends</h4>
                                        <div class="ga-border"></div>
                                        <p class="text-muted text-center"><small><b>Love is beautiful, friendship is better</small></b></p>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="gal-detail thumb">
                                        <a href="#" class="image-popup" title="Screenshot-2">
                                            <img src="https://scontent.fmnl3-3.fna.fbcdn.net/v/t1.15752-9/276970563_545614280514619_5552338418654160484_n.jpg?_nc_cat=103&ccb=1-5&_nc_sid=ae9488&_nc_eui2=AeHBES8rEQlLB-4mEFr-gwDnT-KUeBJU6GtP4pR4ElToaxwltnapu1hPQvc8vuh2UgU-9IU1QnKNtTuORv9PYmfP&_nc_ohc=j7x3Rgk0YmoAX87tL8l&_nc_ht=scontent.fmnl3-3.fna&oh=03_AVKQkMPPJ09ZIe8gXuFROPGDrFL6mYizkihvjS3qfOx3gg&oe=625CC288" class="thumb-img" alt="work-thumbnail" width="250">
                                        </a>
                                        <h4 class="text-center">Schoolmates</h4>
                                        <div class="ga-border"></div>
                                        <p class="text-muted text-center"><small><b>The only reason why I love school is because it's  the place where all my friends are.</b></small></p>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    <!-- end col -->
                </div>
                <!-- end row -->
                    
                    </form>
                </div>
                <!-- end settings content-->

            </div> <!-- end tab-content -->
        </div> <!-- end card-box-->

    </div> <!-- end col -->
</div>
</div>
            </div> 
        </div>
        <footer class="footer">
      <div class="container-fixed bg-primary text-white">
      <center>
      
      <font size="5"><b>Laravel Bootstrap 2022 &COPY;</b></font>
      <hr />
        <font size="4">Cellphone No: 0966-413-6146<br />
        Email Address: edilyndeguzman23@yahoo.com<br />
      </td>
      </tr>
      </table>
      </center>
      </div>
    </footer>

    

<style type="text/css">
body{
    color: #1a202c;
    text-align: left;
    background-color:  #e2e8f0;
    font-size: 20px;    
}
.main-body {
    padding: 15px;
}
.card {
    box-shadow: 0 1px 3px 0 rgba(0,0,0,.1), 0 1px 2px 0 rgba(0,0,0,.06);
}

.card {
    position: relative;
    display: flex;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    background-color: #fff;
    background-clip: border-box;
    border: 0 solid rgba(0,0,0,.125);
    border-radius: .25rem;
}

.card-body {
    flex: 1 1 auto;
    min-height: 1px;
    padding: 1rem;
}

.gutters-sm {
    margin-right: -8px;
    margin-left: -8px;
}

.gutters-sm>.col, .gutters-sm>[class*=col-] {
    padding-right: 8px;
    padding-left: 8px;
}
.mb-3, .my-3 {
    margin-bottom: 1rem!important;
}

.bg-gray-300 {
    background-color: #e2e8f0;
}
.h-100 {
    height: 100%!important;
}
.shadow-none {
    box-shadow: none!important;
}
body{margin-top:20px;
background:#eee;
}

h2 {
    clear: both;
    font-size: 1.8em;
    margin-bottom: 10px;
    padding: 10px 0 10px 30px;
}

h3 > span {
    border-bottom: 2px solid #C2C2C2;
    display: inline-block;
    padding: 0 5px 5px;
}

/* USER PROFILE */
#user-profile h2 {
    padding-right: 15px;
}
#user-profile .profile-status {
  font-size: 0.75em;
  padding-left: 12px;
  margin-top: -10px;
  padding-bottom: 10px;
  color: #8dc859;
}
#user-profile .profile-status.offline {
  color: #fe635f;
}
#user-profile .profile-img {
  border: 1px solid #e1e1e1;
  padding: 4px;
  margin-top: 10px;
  margin-bottom: 10px;
}
#user-profile .profile-label {
  text-align: center;
  padding: 5px 0;
}
#user-profile .profile-label .label {
  padding: 5px 15px;
  font-size: 1em;
}
#user-profile .profile-stars {
  color: #FABA03;
  padding: 7px 0;
  text-align: center;
}
#user-profile .profile-stars > i {
  margin-left: -2px;
}
#user-profile .profile-since {
  text-align: center;
  margin-top: -5px;
}
#user-profile .profile-details {
  padding: 15px 0;
  border-top: 1px solid #e1e1e1;
  border-bottom: 1px solid #e1e1e1;
  margin: 15px 0;
}
#user-profile .profile-details ul {
  padding: 0;
  margin-top: 0;
  margin-bottom: 0;
  margin-left: 40px;
}
#user-profile .profile-details ul > li {
  margin: 3px 0;
  line-height: 1.5;
}
#user-profile .profile-details ul > li > i {
  padding-top: 2px;
}
#user-profile .profile-details ul > li > span {
  color: #34d1be;
}
#user-profile .profile-header {
  position: relative;
}
#user-profile .profile-header > h3 {
  margin-top: 10px
}
#user-profile .profile-header .edit-profile {
  margin-top: -6px;
  position: absolute;
  right: 0;
  top: 0;
}
#user-profile .profile-tabs {
  margin-top: 30px;
}
#user-profile .profile-user-info {
  padding-bottom: 20px;
}
#user-profile .profile-user-info .profile-user-details {
  position: relative;
  padding: 4px 0;
}
#user-profile .profile-user-info .profile-user-details .profile-user-details-label {
  width: 110px;
  float: left;
  bottom: 0;
  font-weight: bold;
  left: 0;
  position: absolute;
  text-align: right;
  top: 0;
  width: 110px;
  padding-top: 4px;
}
#user-profile .profile-user-info .profile-user-details .profile-user-details-value {
  margin-left: 120px;
}
#user-profile .profile-social li {
  padding: 4px 0;
}
#user-profile .profile-social li > i {
  padding-top: 6px;
}
@media only screen and (max-width: 767px) {
  #user-profile .profile-user-info .profile-user-details .profile-user-details-label {
    float: none;
    position: relative;
    text-align: left;
  }
  #user-profile .profile-user-info .profile-user-details .profile-user-details-value {
    margin-left: 0;
  }
  #user-profile .profile-social {
    margin-top: 20px;
  }
}
@media only screen and (max-width: 420px) {
  #user-profile .profile-header .edit-profile {
    display: block;
    position: relative;
    margin-bottom: 15px;
  }
  #user-profile .profile-message-btn .btn {
    display: block;
  }
}


.main-box {
    background: #FFFFFF;
    -webkit-box-shadow: 1px 1px 2px 0 #CCCCCC;
    -moz-box-shadow: 1px 1px 2px 0 #CCCCCC;
    -o-box-shadow: 1px 1px 2px 0 #CCCCCC;
    -ms-box-shadow: 1px 1px 2px 0 #CCCCCC;
    box-shadow: 1px 1px 2px 0 #CCCCCC;
    margin-bottom: 16px;
    padding: 20px;
}
.main-box h2 {
    margin: 0 0 15px -20px;
    padding: 5px 0 5px 20px;
    border-left: 10px solid #c2c2c2; /*7e8c8d*/
}

.btn {
    border: none;
  padding: 6px 12px;
  border-bottom: 4px solid;
  -webkit-transition: border-color 0.1s ease-in-out 0s, background-color 0.1s ease-in-out 0s;
  transition: border-color 0.1s ease-in-out 0s, background-color 0.1s ease-in-out 0s;
  outline: none;
}
.btn-default,
.wizard-cancel,
.wizard-back {
  background-color: #7e8c8d;
  border-color: #626f70;
  color: #fff;
}
.btn-default:hover,
.btn-default:focus,
.btn-default:active,
.btn-default.active, 
.open .dropdown-toggle.btn-default,
.wizard-cancel:hover,
.wizard-cancel:focus,
.wizard-cancel:active,
.wizard-cancel.active,
.wizard-back:hover,
.wizard-back:focus,
.wizard-back:active,
.wizard-back.active {
  background-color: #949e9f;
  border-color: #748182;
  color: #fff;
}
.btn-default .caret {
  border-top-color: #FFFFFF;
}
.btn-info {
  background-color: #5daee7;
  border-color: #4c95c9;
}
.btn-info:hover,
.btn-info:focus,
.btn-info:active,
.btn-info.active, 
.open .dropdown-toggle.btn-info {
  background-color: #4c95c9;
  border-color: #3f80af;
}
.btn-link {
  border: none;
}
.btn-primary {
  background-color: #3fcfbb;
  border-color: #2fb2a0;
}
.btn-primary:hover,
.btn-primary:focus,
.btn-primary:active,
.btn-primary.active, 
.open .dropdown-toggle.btn-primary {
  background-color: #38c2af;
  border-color: #2aa493;
}
.btn-success {
  background-color: #8dc859;
  border-color: #77ab49;
}
.btn-success:hover,
.btn-success:focus,
.btn-success:active,
.btn-success.active, 
.open .dropdown-toggle.btn-success {
  background-color: #77ab49;
}
.btn-danger {
  background-color: #fe635f;
  border-color: #dd504c;
}
.btn-danger:hover,
.btn-danger:focus,
.btn-danger:active,
.btn-danger.active, 
.open .dropdown-toggle.btn-danger {
  background-color: #dd504c;
}
.btn-warning {
  background-color: #f1c40f;
  border-color: #d5ac08;
}
.btn-warning:hover,
.btn-warning:focus,
.btn-warning:active,
.btn-warning.active, 
.open .dropdown-toggle.btn-warning {
  background-color: #e0b50a;
  border-color: #bd9804;
}

.icon-box {
  
}
.icon-box .btn {
  border: 1px solid #e1e1e1;
  margin-left: 3px;
  margin-right: 0;
}
.icon-box .btn:hover {
  background-color: #eee;
  color: #2BB6A3;
}

a {
    color: #2bb6a3;
  outline: none !important;
}
a:hover,
a:focus {
  color: #2bb6a3;
}


/* TABLES */
.table {
    border-collapse: separate;
}
.table-hover > tbody > tr:hover > td,
.table-hover > tbody > tr:hover > th {
  background-color: #eee;
}
.table thead > tr > th {
  border-bottom: 1px solid #C2C2C2;
  padding-bottom: 0;
}
.table tbody > tr > td {
  font-size: 0.875em;
  background: #f5f5f5;
  border-top: 10px solid #fff;
  vertical-align: middle;
  padding: 12px 8px;
}
.table tbody > tr > td:first-child,
.table thead > tr > th:first-child {
  padding-left: 20px;
}
.table thead > tr > th span {
  border-bottom: 2px solid #C2C2C2;
  display: inline-block;
  padding: 0 5px;
  padding-bottom: 5px;
  font-weight: normal;
}
.table thead > tr > th > a span {
  color: #344644;
}
.table thead > tr > th > a span:after {
  content: "\f0dc";
  font-family: FontAwesome;
  font-style: normal;
  font-weight: normal;
  text-decoration: inherit;
  margin-left: 5px;
  font-size: 0.75em;
}
.table thead > tr > th > a.asc span:after {
  content: "\f0dd";
}
.table thead > tr > th > a.desc span:after {
  content: "\f0de";
}
.table thead > tr > th > a:hover span {
  text-decoration: none;
  color: #2bb6a3;
  border-color: #2bb6a3;
}
.table.table-hover tbody > tr > td {
  -webkit-transition: background-color 0.15s ease-in-out 0s;
  transition: background-color 0.15s ease-in-out 0s;
}
.table tbody tr td .call-type {
  display: block;
  font-size: 0.75em;
  text-align: center;
}
.table tbody tr td .first-line {
  line-height: 1.5;
  font-weight: 400;
  font-size: 1.125em;
}
.table tbody tr td .first-line span {
  font-size: 0.875em;
  color: #969696;
  font-weight: 300;
}
.table tbody tr td .second-line {
  font-size: 0.875em;
  line-height: 1.2;
}
.table a.table-link {
  margin: 0 5px;
  font-size: 1.125em;
}
.table a.table-link:hover {
  text-decoration: none;
  color: #2aa493;
}
.table a.table-link.danger {
  color: #fe635f;
}
.table a.table-link.danger:hover {
  color: #dd504c;
}

.table-products tbody > tr > td {
  background: none;
  border: none;
  border-bottom: 1px solid #ebebeb;
  -webkit-transition: background-color 0.15s ease-in-out 0s;
  transition: background-color 0.15s ease-in-out 0s;
  position: relative;
}
.table-products tbody > tr:hover > td {
  text-decoration: none;
  background-color: #f6f6f6;
}
.table-products .name {
  display: block;
  font-weight: 600;
  padding-bottom: 7px;
}
.table-products .price {
  display: block;
  text-decoration: none;
  width: 50%;
  float: left;
  font-size: 0.875em;
}
.table-products .price > i {
  color: #8dc859;
}
.table-products .warranty {
  display: block;
  text-decoration: none;
  width: 50%;
  float: left;
  font-size: 0.875em;
}
.table-products .warranty > i {
  color: #f1c40f;
}
.table tbody > tr.table-line-fb > td {
  background-color: #9daccb;
  color: #262525;
}
.table tbody > tr.table-line-twitter > td {
  background-color: #9fccff;
  color: #262525;
}
.table tbody > tr.table-line-plus > td {
  background-color: #eea59c;
  color: #262525;
}
.table-stats .status-social-icon {
  font-size: 1.9em;
  vertical-align: bottom;
}
.table-stats .table-line-fb .status-social-icon {
  color: #556484;
}
.table-stats .table-line-twitter .status-social-icon {
  color: #5885b8;
}
.table-stats .table-line-plus .status-social-icon {
  color: #a75d54;
}

.daterange-filter {
  background: none repeat scroll 0 0 #FFFFFF;
  border: 1px solid #CCCCCC;
  cursor: pointer;
  padding: 5px 10px;
}
.filter-block .form-group {
  margin-right: 10px;
  position: relative;
}
.filter-block .form-group .form-control {
  height: 36px;
}
.filter-block .form-group .search-icon {
  position: absolute;
  color: #707070;
  right: 8px;
  top: 11px;
}
.filter-block .btn {
  margin-left: 5px;
}
@media only screen and (max-width: 440px) {
  .filter-block {
    float: none !important;
    clear: both
  }
  .filter-block .form-group {
    float: none !important;
    margin-right: 0;
  }
  .filter-block .btn {
    display: block;
    float: none !important;
    margin-bottom: 15px;
    margin-left: 0;
  }
  #reportrange {
    clear: both;
    float: none !important;
    margin-bottom: 15px;
  }
}


.tabs-wrapper .tab-content {
    margin-bottom: 20px;
    padding: 0 10px;
}

/* RECENT - USERS */
.widget-users {
    list-style: none;
  margin: 0;
  padding: 0;
}
.widget-users li {
  border-bottom: 1px solid #ebebeb;
  padding: 15px 0;
  height: 96px;
}
.widget-users li > .img {
  float: left;
  margin-top: 8px;
  width: 50px;
  height: 50px;
  overflow: hidden;
  border-radius: 50%;
}
.widget-users li > .details {
  margin-left: 60px;
}
.widget-users li > .details > .name {
  font-weight: 600;
}
.widget-users li > .details > .name > a {
  color: #344644;
}
.widget-users li > .details > .name > a:hover {
  color: #2bb6a3;
}
.widget-users li > .details > .time {
  color: #2bb6a3;
  font-size: 0.75em;
  padding-bottom: 7px;
}
.widget-users li > .details > .time.online {
  color: #8dc859;
}


/* CONVERSATION */
.conversation-inner {
    padding: 0 0 5px 0;
  margin-right: 10px;
}
.conversation-item {
  padding: 5px 0;
  position: relative;
}
.conversation-user {
  width: 50px;
  height: 50px;
  overflow: hidden;
  float: left;
  border-radius: 50%;
  margin-top: 6px;
}
.conversation-body {
  background: #f5f5f5;
  font-size: 0.875em;
  width: auto;
  margin-left: 60px;
  padding: 8px 10px;
  position: relative;
}
.conversation-body:before {
  border-color: transparent #f5f5f5 transparent transparent;
  border-style: solid;
  border-width: 6px;
  content: "";
  cursor: pointer;
  left: -12px;
  position: absolute;
  top: 25px;
}
.conversation-item.item-right .conversation-body {
  background: #dcfffa;
}
.conversation-item.item-right .conversation-body:before {
  border-color: transparent transparent transparent #dcfffa;
  left: auto;
  right: -12px;
}
.conversation-item.item-right .conversation-user {
  float: right;
}
.conversation-item.item-right .conversation-body {
  margin-left: 0;
  margin-right: 60px;
}
.conversation-body > .name {
  font-weight: 600;
  font-size: 1.125em;
}
.conversation-body > .time {
  position: absolute;
  font-size: 0.875em;
  right: 10px;
  top: 0;
  margin-top: 10px;
  color: #605f5f;
  font-weight: 300;
}
.conversation-body > .time:before {
  content: "\f017";
  font-family: FontAwesome;
  font-style: normal;
  font-weight: normal;
  text-decoration: inherit;
  margin-top: 4px;
  font-size: 0.875em;
}
.conversation-body > .text {
  padding-top: 6px;
}
.conversation-new-message {
  padding-top: 10px;
}

textarea.form-control {
    height: auto;
}
.form-control {
    border-radius: 0px;
    border-color: #e1e1e1;
    box-shadow: none;
    -webkit-box-shadow: none;
}
body{
    color: #6c757d;
    background-color: #f5f6f8;
    margin-top:20px;
}
.card-box {
    background-color: #fff;
    background-clip: border-box;
    border: 1px solid #e7eaed;
    padding: 1.5rem;
    margin-bottom: 24px;
    border-radius: .25rem;
}
.avatar-xl {
    height: 6rem;
    width: 6rem;
}
.rounded-circle {
    border-radius: 50%!important;
}
.nav-pills .nav-link.active, .nav-pills .show>.nav-link {
    color: #fff;
    background-color: #1abc9c;
}
.nav-pills .nav-link {
    border-radius: .25rem;
}
.navtab-bg li>a {
    background-color: #f7f7f7;
    margin: 0 5px;
}
.nav-pills>li>a, .nav-tabs>li>a {
    color: #6c757d;
    font-weight: 600;
}
.mb-4, .my-4 {
    margin-bottom: 2.25rem!important;
}
.tab-content {
    padding: 20px 0 0 0;
}
.progress-sm {
    height: 5px;
}
.m-0 {
    margin: 0!important;
}
.table .thead-light th {
    color: #6c757d;
    background-color: #f1f5f7;
    border-color: #dee2e6;
}
.social-list-item {
    height: 2rem;
    width: 2rem;
    line-height: calc(2rem - 4px);
    display: block;
    border: 2px solid #adb5bd;
    border-radius: 50%;
    color: #adb5bd;
}

.text-purple {
    color: #6559cc!important;
}
.border-purple {
    border-color: #6559cc!important;
}

.timeline {
    margin-bottom: 50px;
    position: relative;
}
.timeline:before {
    background-color: #dee2e6;
    bottom: 0;
    content: "";
    left: 50%;
    position: absolute;
    top: 30px;
    width: 2px;
    z-index: 0;
}
.timeline .time-show {
    margin-bottom: 30px;
    margin-top: 30px;
    position: relative;
}
.timeline .timeline-box {
    background: #fff;
    display: block;
    margin: 15px 0;
    position: relative;
    padding: 20px;
}
.timeline .timeline-album {
    margin-top: 12px;
}
.timeline .timeline-album a {
    display: inline-block;
    margin-right: 5px;
}
.timeline .timeline-album img {
    height: 36px;
    width: auto;
    border-radius: 3px;
}
@media (min-width: 768px) {
    .timeline .time-show {
        margin-right: -69px;
        text-align: right;
    }
    .timeline .timeline-box {
        margin-left: 45px;
    }
    .timeline .timeline-icon {
        background: #dee2e6;
        border-radius: 50%;
        display: block;
        height: 20px;
        left: -54px;
        margin-top: -10px;
        position: absolute;
        text-align: center;
        top: 50%;
        width: 20px;
    }
    .timeline .timeline-icon i {
        color: #98a6ad;
        font-size: 13px;
        position: absolute;
        left: 4px;
    }
    .timeline .timeline-desk {
        display: table-cell;
        vertical-align: top;
        width: 50%;
    }
    .timeline-item {
        display: table-row;
    }
    .timeline-item:before {
        content: "";
        display: block;
        width: 50%;
    }
    .timeline-item .timeline-desk .arrow {
        border-bottom: 12px solid transparent;
        border-right: 12px solid #fff !important;
        border-top: 12px solid transparent;
        display: block;
        height: 0;
        left: -12px;
        margin-top: -12px;
        position: absolute;
        top: 50%;
        width: 0;
    }
    .timeline-item.timeline-item-left:after {
        content: "";
        display: block;
        width: 50%;
    }
    .timeline-item.timeline-item-left .timeline-desk .arrow-alt {
        border-bottom: 12px solid transparent;
        border-left: 12px solid #fff !important;
        border-top: 12px solid transparent;
        display: block;
        height: 0;
        left: auto;
        margin-top: -12px;
        position: absolute;
        right: -12px;
        top: 50%;
        width: 0;
    }
    .timeline-item.timeline-item-left .timeline-desk .album {
        float: right;
        margin-top: 20px;
    }
    .timeline-item.timeline-item-left .timeline-desk .album a {
        float: right;
        margin-left: 5px;
    }
    .timeline-item.timeline-item-left .timeline-icon {
        left: auto;
        right: -56px;
    }
    .timeline-item.timeline-item-left:before {
        display: none;
    }
    .timeline-item.timeline-item-left .timeline-box {
        margin-right: 45px;
        margin-left: 0;
        text-align: right;
    }
}
@media (max-width: 767.98px) {
    .timeline .time-show {
        text-align: center;
        position: relative;
    }
    .timeline .timeline-icon {
        display: none;
    }
}
.timeline-sm {
    padding-left: 110px;
}
.timeline-sm .timeline-sm-item {
    position: relative;
    padding-bottom: 20px;
    padding-left: 40px;
    border-left: 2px solid #dee2e6;
}
.timeline-sm .timeline-sm-item:after {
    content: "";
    display: block;
    position: absolute;
    top: 3px;
    left: -7px;
    width: 12px;
    height: 12px;
    border-radius: 50%;
    background: #fff;
    border: 2px solid #1abc9c;
}
.timeline-sm .timeline-sm-item .timeline-sm-date {
    position: absolute;
    left: -104px;
}
@media (max-width: 420px) {
    .timeline-sm {
        padding-left: 0;
    }
    .timeline-sm .timeline-sm-date {
        position: relative !important;
        display: block;
        left: 0 !important;
        margin-bottom: 10px;
    }
}


</style>

<script type="text/javascript">

</script>
</body>
</html>

@endsection