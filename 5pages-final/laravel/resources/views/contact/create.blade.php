@extends('layouts.app')

@section('title','Contact Us')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Contact Us') }}</div>

                <div class="card-body">
@if(Session::has('contact'))
<div class="alert alert-danger" role="alert">
  {{Session::get('contact')}}
</div>
@endif
<form action="/contact" method="POST">
 <div class="row mb-3">
     <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Name') }}</label>

     <div class="col-md-6">
      <input id="name" type="name" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

       @error('email')
         <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
            </span>
            @enderror
       </div>
 </div>
 <div class="row mb-3">
     <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Email Address') }}</label>

     <div class="col-md-6">
      <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

       @error('email')
         <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
            </span>
            @enderror
       </div>
 </div>
<div class="row mb-3">
     <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Message') }}</label>

     <div class="col-md-6">
      <textarea id="message" type="message" class="form-control @error('message') is-invalid @enderror" name="message" value="{{ old('message') }}" required autocomplete="message" autofocus></textarea>

       @error('message')
         <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
            </span>
            @enderror
       </div>
 </div>

@csrf
		<div class="row mb-0">
        <div class="col-md-8 offset-md-4">
         <button type="submit" class="btn btn-primary">
                                    {{ __('Send Message') }}
                                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<br>
   <br>
   <br>
   <br>
   <br>
   <br>
   <br>
   <br>
   <br>
   <br>
      <footer class="footer">
      <div class="container-fixed bg-primary text-white">
      <center>
      
      <font size="5"><b>Laravel Bootstrap 2022 &COPY;</b></font>
      <hr />
        <font size="4">Cellphone No: 0966-413-6146<br />
        Email Address: edilyndeguzman23@yahoo.com<br />
      </td>
      </tr>
      </table>
      </center>
      </div>
    </footer>
    <style>
        .navbar-nav.navbar-center {
            position: absolute;
            left: 50%;
            transform: translatex(-50%);
        }
    </style><!-- End Resume Section -->
<style type="text/css">
body{
    color: #1a202c;
    text-align: left;
    background-color:  #e2e8f0; 
    font-size: 20px;   
}
.main-body {
    padding: 15px;
}
.card {
    box-shadow: 0 1px 3px 0 rgba(0,0,0,.1), 0 1px 2px 0 rgba(0,0,0,.06);
}

.card {
    position: relative;
    display: flex;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    background-color: #fff;
    background-clip: border-box;
    border: 0 solid rgba(0,0,0,.125);
    border-radius: .25rem;
}

.card-body {
    flex: 1 1 auto;
    min-height: 1px;
    padding: 1rem;
}

.gutters-sm {
    margin-right: -8px;
    margin-left: -8px;
}

.gutters-sm>.col, .gutters-sm>[class*=col-] {
    padding-right: 8px;
    padding-left: 8px;
}
.mb-3, .my-3 {
    margin-bottom: 1rem!important;
}

.bg-gray-300 {
    background-color: #e2e8f0;
}
.h-100 {
    height: 100%!important;
}
.shadow-none {
    box-shadow: none!important;
}
form{
    margin-top: 20px
}
button{
    margin-left: 100px;
    width: 200px;
}
.btn-link
{
   margin-left: 110px;  
}


</style>

<script type="text/javascript">

</script>
@endsection