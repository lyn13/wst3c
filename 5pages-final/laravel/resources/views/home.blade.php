@extends('layouts.app')
@section('content')
<!-- Masthead-->
<div class="jumbotron text-center" style="margin-bottom:0">
    <div class="container d-flex align-items-center flex-column">
    <div class="col-md-4 col-xs-10 col-sm-6 col-lg-4">
                          <img src="https://scontent.fmnl3-1.fna.fbcdn.net/v/t39.30808-6/219875448_1941150512718200_6276753955770293138_n.jpg?_nc_cat=108&ccb=1-5&_nc_sid=174925&_nc_eui2=AeGjLwwNpci_Q1cpSX0LpLC3fZYFEWt2FOd9lgURa3YU5zjcXQLAgp_oKOuGdx4PbCM9kwFNkhTzDpe90CivE8R3&_nc_ohc=PtkVrnlQQqAAX_tcY4B&tn=5VWzK6HzSt2MpkIz&_nc_zt=23&_nc_ht=scontent.fmnl3-1.fna&oh=00_AT8GClAxuf614oHfvfY4m_H7AddTLPI0JHoeMu1FFVlL8Q&oe=6238E95C" alt="Admin" class="rounded-circle" width="250">
                      </div>
  <h1>Hello, Im</h1>
  <h1 class="masthead-heading text-uppercase mb-0 ">Edilyn R. De Guzman</h1>
  <p class="masthead-subheading font-weight-light mb-0">Information Technology Student</p>
</div>
</div>
</section>
<div class="text-center mb-5">
    <a class="btn btn-primary btn-lg" href="/about">GET STARTED</a>
       </div>
        <!-- Portfolio Section-->
        <section class="page-section portfolio" id="portfolio">
            <div class="container">
                <!-- Portfolio Section Heading-->
                <h2 class="page-section-heading text-center text-uppercase text-secondary mb-0">HOBBIES</h2>
                <!-- Icon Divider-->
                  <hr class="rounded">
                <!-- Portfolio Grid Items-->
                <div class="row justify-content-center">
                    <!-- Portfolio Item 1-->
                    <div class="col-md-6 col-lg-4 mb-5">
                        <div class="portfolio-item mx-auto" data-bs-toggle="modal" data-bs-target="#portfolioModal1">
                            <div class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
                                <div class="portfolio-item-caption-content text-center text-white"><i class="fas fa-plus fa-3x"></i></div>
                            </div>
                            <img class="img-fluid" src="https://scontent.fmnl3-3.fna.fbcdn.net/v/t39.30808-6/275736024_4824207970989776_4441298849029723419_n.jpg?_nc_cat=103&ccb=1-5&_nc_sid=730e14&_nc_eui2=AeGbqp8Vcm1qbW_MGSQ8ZL91ZYdc11oswhdlh1zXWizCF_WMv0nkj2biMwEIfq9kCCEOy3nilfxmyB3fJwkafP67&_nc_ohc=4d1DW2RxigYAX8GY97I&_nc_zt=23&_nc_ht=scontent.fmnl3-3.fna&oh=00_AT92pnaPjtcnv4kB8_Eqc8RD5Qj49bxNI0LUjSEFm1vsTA&oe=623995A3" alt="..." width="250" height="400" />
                        </div>
                    </div>
                    <!-- Portfolio Item 2-->
                    <div class="col-md-6 col-lg-4 mb-5">
                        <div class="portfolio-item mx-auto" data-bs-toggle="modal" data-bs-target="#portfolioModal2">
                            <div class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
                                <div class="portfolio-item-caption-content text-center text-white"><i class="fas fa-plus fa-3x"></i></div>
                            </div>
                            <img class="img-fluid" src="https://scontent.fmnl3-4.fna.fbcdn.net/v/t1.15752-9/275610765_485486959724426_8287199800246477058_n.jpg?_nc_cat=104&ccb=1-5&_nc_sid=ae9488&_nc_eui2=AeFdtPH4spwF4FOelPLwBrI5xcXCMqMuO9PFxcIyoy470wm_RJwVLxYzF7ow1swYtwagedRaarAL6gYS1xlEukY7&_nc_ohc=M4qBf-671lEAX8qVQMR&_nc_ht=scontent.fmnl3-4.fna&oh=03_AVJ6kYRYQ_dc6ip3zFrVVBbngQmT9d00gzXzX3zlySt65w&oe=625945AF" alt="..." width="232" height="300" />
                        </div>
                    </div>
                    <!-- Portfolio Item 3-->
                    <div class="col-md-6 col-lg-4 mb-5">
                        <div class="portfolio-item mx-auto" data-bs-toggle="modal" data-bs-target="#portfolioModal3">
                            <div class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
                                <div class="portfolio-item-caption-content text-center text-white"><i class="fas fa-plus fa-3x"></i></div>
                            </div>
                            <img class="img-fluid" src="https://scontent.xx.fbcdn.net/v/t1.15752-9/275400241_543730253846674_6202195567174970975_n.png?stp=dst-png_p75x225&_nc_cat=101&ccb=1-5&_nc_sid=aee45a&_nc_eui2=AeH6YJzMqSfpse6d4vKCnuhEOn8NlvBbBTo6fw2W8FsFOiGiX40ehginvsu9LEYAgwdodTunMrE5iNgr5-v1sKp6&_nc_ohc=Uj9kY3Mdv3gAX9oBXGO&_nc_ad=z-m&_nc_cid=0&_nc_ht=scontent.xx&oh=03_AVJrbeRujO7s0p7ZEOOq507s3bsotmZ-3j-TtVDQpSZeWA&oe=625B4547" alt="..." width="232" height="300" />
                        </div>
                    </div>
                    <!-- Portfolio Item 4-->
                    <div class="col-md-6 col-lg-4 mb-5 mb-lg-0">
                        <div class="portfolio-item mx-auto" data-bs-toggle="modal" data-bs-target="#portfolioModal4">
                            <div class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
                                <div class="portfolio-item-caption-content text-center text-white"><i class="fas fa-plus fa-3x"></i></div>
                            </div>
                            <img class="img-fluid" src="https://cdn5.eyeem.com/thumb/820b2264308af28ff02eb910db0653abe8e24a5e-1609914493226/w/800" alt="..."  width="232" height="300" />
                        </div>
                    </div>
                    <!-- Portfolio Item 5-->
                    <div class="col-md-6 col-lg-4 mb-5 mb-md-0">
                        <div class="portfolio-item mx-auto" data-bs-toggle="modal" data-bs-target="#portfolioModal5">
                            <div class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
                                <div class="portfolio-item-caption-content text-center text-white"><i class="fas fa-plus fa-3x"></i></div>
                            </div>
                            <img class="img-fluid" src="https://i.pinimg.com/originals/73/8b/6d/738b6d329014692d357e4b6a8180d9e8.jpg" alt="..." width="232" height="300" />
                        </div>
                    </div>
                    <!-- Portfolio Item 6-->
                    <div class="col-md-6 col-lg-4">
                        <div class="portfolio-item mx-auto" data-bs-toggle="modal" data-bs-target="#portfolioModal6">
                            <div class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
                                <div class="portfolio-item-caption-content text-center text-white"><i class="fas fa-plus fa-3x"></i></div>
                            </div>
                            <img class="img-fluid" src="https://images.unsplash.com/photo-1603284569248-821525309698?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxleHBsb3JlLWZlZWR8OHx8fGVufDB8fHx8&w=1000&q=80" alt="..." width="232" height="300" />
                        </div>

                    </div>
                    <!-- Icon Divider-->
                <div class="divider-custom">
                  <hr class="rounded">
                    <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                    <div class="divider-custom-line"></div>
                </div>
                </div>
            </div>
        </section>
    </div>

        <!-- About Section-->
        <section class="page-section bg-dark blue text-white mb-0" id="about">
            <div class="container">
                <!-- About Section Heading-->
                <h2 class="page-section-heading text-center text-uppercase text-primary"><b>MORE ABOUT ME </b></h2>
                <!-- Icon Divider-->
                <div class="divider-custom divider-light">
                </div>
                <!-- ======= Resume Section ======= -->
    <section id="resume" class="resume">
      <div class="container" data-aos="fade-up">
        <div class="row">
          <div class="col-lg-6">
            <h3 class="resume-title">FAVORITE BIBLE VERSE</h3>
            <div class="resume-item pb-0">
              <ul>
                <li><b>Romans 8:28</b> And we know that in all things God works for the good of those who love him, who[a] have been called according to his purpose.</li>
                <li><b>Proverbs 3:5–6</b>Trust in the LORD with all your heart, and do not lean on your own understanding. In all your ways acknowledge Him, and He will make straight your paths.</li>
                <li><b>Jeremiah 29:11</b>
                For I know the plans I have for you,” declares the LORD, “plans to prosper you and not to harm you, plans to give you hope and a future.</li>
              </ul>
            </div>
            <div class="resume-item">
            </div>
          </div>
          <div class="col-lg-6">
            <h3 class="resume-title">GOALS IN LIFE</h3>
            <div class="resume-item">
              <ul>
                <li>Follow my dreams</li>
                <li>Do different things</li>
                <li>Master a five languages</li>
                <li>Learn more</li>
                <li>Volunteer</li>
                <li>Start a business</li>
                <li>Buy my dream home/house</li>
                <li>Travel around the world </li>
                 <li>Marriage and Family Harmony</li>
              </ul>
            </div>
            
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>
      <footer class="footer">
      <div class="container-fixed bg-primary text-white">
      <center>
      
      <font size="5"><b>Laravel Bootstrap 2022 &COPY;</b></font>
      <hr />
        <font size="4">Cellphone No: 0966-413-6146<br />
        Email Address: edilyndeguzman23@yahoo.com<br />
      </td>
      </tr>
      </table>
      </center>
      </div>
    </footer>
    <style>
        .navbar-nav.navbar-center {
            position: absolute;
            left: 50%;
            transform: translatex(-50%);
        }
    </style><!-- End Resume Section -->
<style type="text/css">
body{
    color: #1a202c;
    text-align: left;
    background-color:  #e2e8f0; 
    font-size: 20px;   
}
.main-body {
    padding: 15px;
}
.card {
    box-shadow: 0 1px 3px 0 rgba(0,0,0,.1), 0 1px 2px 0 rgba(0,0,0,.06);
}

.card {
    position: relative;
    display: flex;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    background-color: #fff;
    background-clip: border-box;
    border: 0 solid rgba(0,0,0,.125);
    border-radius: .25rem;
}

.card-body {
    flex: 1 1 auto;
    min-height: 1px;
    padding: 1rem;
}

.gutters-sm {
    margin-right: -8px;
    margin-left: -8px;
}

.gutters-sm>.col, .gutters-sm>[class*=col-] {
    padding-right: 8px;
    padding-left: 8px;
}
.mb-3, .my-3 {
    margin-bottom: 1rem!important;
}

.bg-gray-300 {
    background-color: #e2e8f0;
}
.h-100 {
    height: 100%!important;
}
.shadow-none {
    box-shadow: none!important;
}
.breadcrumb {
   background-color: #2873ed;
   color: #ffffff;
}


</style>

<script type="text/javascript">

</script>
</body>
</html>
@endsection
