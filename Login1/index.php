<?php

require_once 'config.php';

$permissions = ['email']; //optional

if (isset($accessToken))
{
	if (!isset($_SESSION['facebook_access_token'])) 
	{
		//get short-lived access token
		$_SESSION['facebook_access_token'] = (string) $accessToken;
		
		//OAuth 2.0 client handler
		$oAuth2Client = $fb->getOAuth2Client();
		
		//Exchanges a short-lived access token for a long-lived one
		$longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($_SESSION['facebook_access_token']);
		$_SESSION['facebook_access_token'] = (string) $longLivedAccessToken;
		
		//setting default access token to be used in script
		$fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
	} 
	else 
	{
		$fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
	}
	
	
	//redirect the user to the index page if it has $_GET['code']
	if (isset($_GET['code'])) 
	{
		header('Location: ./');
	}
	
	
	try {
		$fb_response = $fb->get('/me?fields=name,email');
		$fb_response_picture = $fb->get('/me/picture?redirect=false&height=200');
		
		$fb_user = $fb_response->getGraphUser();
		$picture = $fb_response_picture->getGraphUser();
		
		$_SESSION['fb_user_id'] = $fb_user->getProperty('id');
		$_SESSION['fb_user_name'] = $fb_user->getProperty('name');
		$_SESSION['fb_user_email'] = $fb_user->getProperty('email');
		$_SESSION['fb_user_pic'] = $picture['url'];
		
		
	} catch(Facebook\Exceptions\FacebookResponseException $e) {
		echo 'Facebook API Error: ' . $e->getMessage();
		session_destroy();
		// redirecting user back to app login page
		header("Location: ./");
		exit;
	} catch(Facebook\Exceptions\FacebookSDKException $e) {
		echo 'Facebook SDK Error: ' . $e->getMessage();
		exit;
	}
} 
else 
{	
	// replace your website URL same as added in the developers.Facebook.com/apps e.g. if you used http instead of https and you used
	$fb_login_url = $fb_helper->getLoginUrl('http://localhost/project/wst3c/wst3c/Login1/', $permissions);
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Login with Facebook</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
  
  <link href="<?php echo BASE_URL; ?>css/style.css" rel="stylesheet">
  
</head>
<body>
	<style>
		h3{
			font-size: 50px;
			font-family: ;
		}
		form{
			margin-left: 570px;
			margin-right: 500px;
			margin-top: 250px;
		}
		h1{
			margin-right: 60px;
			margin-top: 10px;
		}
	</style>


<!-- NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN --> 
<!--  If the user is login  -->
<?php if(isset($_SESSION['fb_user_id'])): ?>
	<nav class="navbar navbar-expand-sm bg-primary navbar-dark">
	  <a class="navbar-brand" href="<?php echo BASE_URL; ?>">HOME</a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
		<span class="navbar-toggler-icon"></span>
	  </button>
	  <div class="collapse navbar-collapse" id="collapsibleNavbar">
		<ul class="navbar-nav">
		  <li class="nav-item">
			<a class="nav-link" href="logout.php">Logout</a>
		  </li>    
		</ul>
	  </div>  
	</nav>

	<div class="container" style="margin-top:30px">
		  <h3>User Information</h3>
		  <ul class="nav nav-pills flex-column">
			<li class="nav-item"><b>Facebook ID: </b><?php echo  $_SESSION['fb_user_id']; ?>
			<li class="nav-item"><b>Name: </b><?php echo $_SESSION['fb_user_name']; ?></a>
			</li>
			<li class="nav-item"><b>Email: </b><?php echo $_SESSION['fb_user_email']; ?></a>
			</li>
			<li class="nav-item"><b>Year & Section: </b> 3C</a>
			</li>
			<li class="nav-item"><b>Date: </b> March 06,2022</a>
			</li>
			<li class="nav-item"><b>Time: </b> 09:00 PM</a>
			</li>
		  </ul>
		  
		</div>
	  </div>
	</div>
<!-- NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN --> 
<!-- if user not login --> 
<?php else: ?>
 
<form action="index.php" method="get"> 
        <div class="row">
        <div class="col-md-5 p-1">
        <label>Name:</label><input type="text" class="form-control" value="Edilyn Rilloma De Guzman" name="name" readonly>
        </div>
        <div class="col-md-5 p-1">
        <label>Date:</label><input type="text" class="form-control" value="March 06, 2022" name="date"readonly>
        </div>
        <div class="col-md-5 p-1">
        <label>Year & Section:</label><input type="text" class="form-control" value="3C" name="ys" readonly>
        </div>
        <div class="col-md-5 p-1">
        <label>Time:</label><input type="text" class="form-control" value="09:00 PM" name="time" readonly>
        </div>
  		<br>
  		<br>
  		<br>
  		<div class="col-md-10 p-3">
			<div class="text-center social-btn ">
				<a href="<?php echo $fb_login_url;?>" class="btn btn-primary btn-block "><i class="fa fa-facebook"></i> <b>Login with Facebook</b></a>
				<a href="<?php echo $fb_login_url;?>" class="btn btn-danger btn-block "><i class="fa fa-google-plus"></i> <b>Login with Facebook</b></a>
			</div>
		</form>
			
<?php endif ?>
<!-- NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN -->
</body>
</html>