-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 19, 2022 at 09:40 AM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hangryboss`
--

-- --------------------------------------------------------

--
-- Table structure for table `abouts`
--

CREATE TABLE `abouts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `description1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `abouts`
--

INSERT INTO `abouts` (`id`, `description1`, `description2`, `description3`, `image`, `created_at`, `updated_at`) VALUES
(4, 'Welcome to HANGRY BOSS. A place where you can enjoy foods and drinks within your heart\'s content. Since 2022 Hungry Boss has been serving people, young and adult, orders the food they want to eat in such a small amount of price to be paid off.', 'We help them to experience a stress-free and a friendly environment with their friends, colleagues and only by themselves to get away from their daily lives and load of works.', 'And our main goal is to bring happiness and satisfy our customers\' cravings on the foods and drinks that can help them complete their days.', '1655623381.jpg', '2022-06-18 21:19:00', '2022-06-18 23:23:01');

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `food_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `carts`
--

INSERT INTO `carts` (`id`, `user_id`, `food_id`, `quantity`, `created_at`, `updated_at`) VALUES
(15, '5', '9', '5', '2022-05-24 16:53:42', '2022-05-24 16:53:42'),
(17, '5', '19', '4', '2022-05-24 16:56:08', '2022-05-24 16:56:08'),
(18, '5', '27', '3', '2022-05-24 16:56:24', '2022-05-24 16:56:24'),
(19, '7', '10', '1', '2022-06-04 18:38:55', '2022-06-04 18:38:55'),
(20, '7', '11', '1', '2022-06-04 18:39:04', '2022-06-04 18:39:04'),
(21, '7', '16', '1', '2022-06-04 18:39:14', '2022-06-04 18:39:14'),
(22, '7', '27', '1', '2022-06-04 18:39:30', '2022-06-04 18:39:30'),
(23, '5', '19', '1', '2022-06-06 01:33:59', '2022-06-06 01:33:59'),
(24, '5', '33', '2', '2022-06-06 02:24:00', '2022-06-06 02:24:00'),
(39, '12', '22', '2', '2022-06-15 00:04:32', '2022-06-15 00:04:32'),
(40, '12', '8', '3', '2022-06-15 00:04:42', '2022-06-15 00:04:42'),
(41, '12', '15', '3', '2022-06-15 00:04:52', '2022-06-15 00:04:52'),
(42, '12', '26', '3', '2022-06-15 00:05:02', '2022-06-15 00:05:02'),
(43, '12', '26', '4', '2022-06-15 00:05:19', '2022-06-15 00:05:19'),
(44, '20', '11', '3', '2022-06-16 02:26:19', '2022-06-16 02:26:19'),
(45, '20', '11', '1', '2022-06-16 02:26:32', '2022-06-16 02:26:32'),
(46, '20', '12', '1', '2022-06-16 02:26:54', '2022-06-16 02:26:54'),
(47, '20', '11', '1', '2022-06-16 02:27:03', '2022-06-16 02:27:03'),
(48, '20', '11', '1', '2022-06-16 02:27:11', '2022-06-16 02:27:11'),
(54, '22', '9', '3', '2022-06-18 19:33:49', '2022-06-18 19:33:49'),
(55, '22', '13', '1', '2022-06-18 19:33:57', '2022-06-18 19:33:57'),
(56, '22', '17', '1', '2022-06-18 19:34:04', '2022-06-18 19:34:04'),
(57, '22', '26', '1', '2022-06-18 19:34:14', '2022-06-18 19:34:14'),
(58, '22', '18', '1', '2022-06-18 19:34:22', '2022-06-18 19:34:22'),
(64, '8', '7', '1', '2022-06-18 23:33:15', '2022-06-18 23:33:15'),
(65, '8', '14', '1', '2022-06-18 23:33:23', '2022-06-18 23:33:23'),
(66, '8', '17', '1', '2022-06-18 23:33:30', '2022-06-18 23:33:30'),
(67, '8', '22', '1', '2022-06-18 23:33:41', '2022-06-18 23:33:41'),
(68, '8', '26', '1', '2022-06-18 23:33:53', '2022-06-18 23:33:53'),
(70, '9', '23', '4', '2022-06-18 23:37:52', '2022-06-18 23:37:52'),
(71, '9', '13', '1', '2022-06-18 23:38:01', '2022-06-18 23:38:01'),
(72, '9', '14', '1', '2022-06-18 23:38:10', '2022-06-18 23:38:10'),
(73, '9', '18', '1', '2022-06-18 23:38:17', '2022-06-18 23:38:17'),
(74, '9', '26', '1', '2022-06-18 23:38:30', '2022-06-18 23:38:30');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `food`
--

CREATE TABLE `food` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `food`
--

INSERT INTO `food` (`id`, `title`, `price`, `image`, `description`, `created_at`, `updated_at`) VALUES
(7, 'Buttered Cream', '75.00', '1655476995.jpg', 'Chicken Wings', '2022-05-17 16:54:13', '2022-06-17 06:43:15'),
(8, 'Buffalo Wings', '75.00', '1655623851.jpg', 'Chicken Wings', '2022-05-17 16:58:07', '2022-06-18 23:30:51'),
(9, 'Nutty Sriracha', '75.00', '1652868608.jpg', 'Chicken Wings', '2022-05-17 16:58:58', '2022-05-18 02:10:08'),
(10, 'Lemon Zest', '75.00', '1652868632.jpg', 'Chicken Wings', '2022-05-17 16:59:37', '2022-05-18 02:10:32'),
(11, 'Orange Glazed', '75.00', '1652868670.jpg', 'Chicken Wings', '2022-05-17 17:01:35', '2022-05-18 02:11:10'),
(12, 'Mango Habanero', '75.00', '1652868693.jpg', 'Chicken Wings', '2022-05-17 17:02:09', '2022-05-18 02:11:33'),
(13, 'Tonkotsu', '150.00', '1652868847.jpg', 'Noodle Soup', '2022-05-17 17:10:33', '2022-05-18 02:14:07'),
(14, 'Spicy Tantamen', '150.00', '1652868990.jpg', 'Noodle Soup', '2022-05-17 17:11:34', '2022-05-18 02:16:30'),
(15, 'Cheesy Bacon', '180.00', '1652869494.jpg', 'Pizza', '2022-05-17 17:44:21', '2022-05-18 02:24:54'),
(16, 'Pepperoni Madness', '150.00', '1652869524.jpg', 'Pizza', '2022-05-17 17:47:52', '2022-05-18 02:25:24'),
(17, 'Hawaiian Overload', '150.00', '1652869551.jpg', 'Pizza', '2022-05-17 17:48:41', '2022-05-18 02:25:51'),
(18, 'Smoked Hotdog', '55.00', '1652869667.jpg', 'Hotdog Buns', '2022-05-18 01:24:32', '2022-05-18 02:27:47'),
(19, 'Bacon & Egg', '65.00', '1652869700.jpg', 'Hotdog Buns', '2022-05-18 01:27:48', '2022-05-18 02:28:20'),
(20, 'Chicken Mushroom', '80.00', '1652869729.jpg', 'Hotdog Buns', '2022-05-18 01:28:37', '2022-05-18 02:28:49'),
(21, 'Garlic Steak', '90.00', '1652869750.jpg', 'Hotdog Buns', '2022-05-18 01:29:22', '2022-05-18 02:29:10'),
(22, 'Solo', '75.00', '1652875466.jpg', 'Nachos', '2022-05-18 04:01:47', '2022-05-18 04:04:26'),
(23, 'Barkada Pack', '120.00', '1652875522.jpg', 'Nachos', '2022-05-18 04:05:22', '2022-05-18 04:05:22'),
(24, 'Family Pack', '250.00', '1652875552.jpg', 'Nachos', '2022-05-18 04:05:52', '2022-05-18 04:05:52'),
(25, 'Family Pack', '250.00', '1652875580.jpg', 'Nachos', '2022-05-18 04:06:20', '2022-05-18 04:06:20'),
(26, 'Nutella Banana', '95.00', '1652939872.jpg', 'Milk Tea', '2022-05-18 21:57:52', '2022-05-18 21:57:52'),
(27, 'Nutella Hazelnut', '95.00', '1652939941.jpg', 'Milk Tea', '2022-05-18 21:59:01', '2022-05-18 21:59:01'),
(28, 'Oreo Chocolate', '90.00', '1652939974.jpg', 'Milk Tea', '2022-05-18 21:59:34', '2022-05-18 21:59:34'),
(29, 'White Rabbit Taro', '90.00', '1652940020.jpg', 'Milk Tea', '2022-05-18 22:00:20', '2022-05-18 22:00:20'),
(30, 'Crunch Chocolate', '95.00', '1652940065.jpg', 'Milk Tea', '2022-05-18 22:01:05', '2022-05-18 22:01:05'),
(31, 'Kitkat Matcha Cheesecake', '100.00', '1652940185.jpg', 'Milk Tea', '2022-05-18 22:03:05', '2022-05-18 22:03:05'),
(32, 'Nutella Hazelnut', '155.00', '1652940255.jpg', 'Milk Tea', '2022-05-18 22:04:15', '2022-05-18 22:04:15'),
(33, 'Strawberry Yakult', '75.00', '1652940327.jpg', 'Milk Tea', '2022-05-18 22:05:27', '2022-05-18 22:05:27'),
(34, 'Dark Choco Cheesecake', '140.00', '1652940367.jpg', 'Milk Tea', '2022-05-18 22:06:07', '2022-05-18 22:06:07'),
(35, 'Red Velvet Cheesecake', '140.00', '1652940410.jpg', 'Milk Tea', '2022-05-18 22:06:50', '2022-05-18 22:06:50'),
(36, 'Barkada Pack', '420.00', '1652941569.jpg', 'Chicken Wings', '2022-05-18 22:26:09', '2022-05-18 22:26:09'),
(37, 'Barkada Pack', '420.00', '1652941596.jpg', 'Chicken Wings', '2022-05-18 22:26:36', '2022-05-18 22:26:36');

-- --------------------------------------------------------

--
-- Table structure for table `foodchefs`
--

CREATE TABLE `foodchefs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `speciality` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `foodchefs`
--

INSERT INTO `foodchefs` (`id`, `name`, `speciality`, `image`, `created_at`, `updated_at`) VALUES
(3, 'Ivan Tamayo', 'Barista', '1654500515.jpg', '2022-04-22 01:34:48', '2022-06-05 23:28:35'),
(4, 'Rowella Cancino', 'Head Chef', '1654500099.jpg', '2022-04-22 01:36:16', '2022-06-05 23:21:39'),
(7, 'Rizzel Mae Ayo', 'Sous Chef/Waiter', '1655623957.jpg', '2022-06-18 23:32:37', '2022-06-18 23:32:37');

-- --------------------------------------------------------

--
-- Table structure for table `landingpages`
--

CREATE TABLE `landingpages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `landingpages`
--

INSERT INTO `landingpages` (`id`, `image`, `created_at`, `updated_at`) VALUES
(7, '1655623331.jpg', '2022-06-18 23:22:11', '2022-06-18 23:22:11'),
(8, '1655623341.jpg', '2022-06-18 23:22:21', '2022-06-18 23:22:21'),
(9, '1655623348.jpg', '2022-06-18 23:22:28', '2022-06-18 23:22:28'),
(10, '1655623355.jpg', '2022-06-18 23:22:35', '2022-06-18 23:22:35'),
(11, '1655623362.jpg', '2022-06-18 23:22:42', '2022-06-18 23:22:42');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2014_10_12_200000_add_two_factor_columns_to_users_table', 1),
(4, '2019_08_19_000000_create_failed_jobs_table', 1),
(5, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(6, '2022_04_17_080659_create_sessions_table', 1),
(7, '2022_04_18_133933_create_food_table', 2),
(8, '2022_04_22_011055_create_reservations_table', 3),
(9, '2022_04_22_034646_create_foodchefs_table', 4),
(10, '2022_04_23_105738_create_carts_table', 5),
(11, '2022_05_02_134601_create_orders_table', 6),
(12, '2022_06_07_050922_create_orders_table', 7),
(13, '2022_06_17_234725_create_abouts_table', 8),
(14, '2022_06_18_024732_create_abouts_table', 9),
(15, '2022_06_18_042727_create_abouts_table', 10),
(16, '2022_06_18_050821_create_abouts_table', 11),
(17, '2022_06_18_101242_create_landingpages_table', 12),
(18, '2022_06_19_032655_create_orders_table', 13);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `foodname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `foodname`, `price`, `quantity`, `name`, `phone`, `address`, `date`, `time`, `created_at`, `updated_at`) VALUES
(6, 'Buttered Cream', '75.00', '1', 'Edilyn Rilloma De Guzman', '09664136146', 'Turac Malay Pogus San Carlos Pangasinan', '2022-06-18', '17:00', '2022-06-18 23:34:22', '2022-06-18 23:34:22'),
(7, 'Spicy Tantamen', '150.00', '1', 'Edilyn Rilloma De Guzman', '09664136146', 'Turac Malay Pogus San Carlos Pangasinan', '2022-06-18', '17:00', '2022-06-18 23:34:22', '2022-06-18 23:34:22'),
(8, 'Hawaiian Overload', '150.00', '1', 'Edilyn Rilloma De Guzman', '09664136146', 'Turac Malay Pogus San Carlos Pangasinan', '2022-06-18', '17:00', '2022-06-18 23:34:22', '2022-06-18 23:34:22'),
(9, 'Solo', '75.00', '1', 'Edilyn Rilloma De Guzman', '09664136146', 'Turac Malay Pogus San Carlos Pangasinan', '2022-06-18', '17:00', '2022-06-18 23:34:22', '2022-06-18 23:34:22'),
(10, 'Nutella Banana', '95.00', '1', 'Edilyn Rilloma De Guzman', '09664136146', 'Turac Malay Pogus San Carlos Pangasinan', '2022-06-18', '17:00', '2022-06-18 23:34:22', '2022-06-18 23:34:22'),
(11, 'Barkada Pack', '120.00', '4', 'Mark Philip Poquiz', '09291245795', 'Turac San Carlos Pangasinan', '2022-06-19', '08:00', '2022-06-18 23:39:05', '2022-06-18 23:39:05'),
(12, 'Tonkotsu', '150.00', '1', 'Mark Philip Poquiz', '09291245795', 'Turac San Carlos Pangasinan', '2022-06-19', '08:00', '2022-06-18 23:39:05', '2022-06-18 23:39:05'),
(13, 'Spicy Tantamen', '150.00', '1', 'Mark Philip Poquiz', '09291245795', 'Turac San Carlos Pangasinan', '2022-06-19', '08:00', '2022-06-18 23:39:05', '2022-06-18 23:39:05'),
(14, 'Smoked Hotdog', '55.00', '1', 'Mark Philip Poquiz', '09291245795', 'Turac San Carlos Pangasinan', '2022-06-19', '08:00', '2022-06-18 23:39:05', '2022-06-18 23:39:05'),
(15, 'Nutella Banana', '95.00', '1', 'Mark Philip Poquiz', '09291245795', 'Turac San Carlos Pangasinan', '2022-06-19', '08:00', '2022-06-18 23:39:05', '2022-06-18 23:39:05');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `reservations`
--

CREATE TABLE `reservations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guest` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reservations`
--

INSERT INTO `reservations` (`id`, `name`, `email`, `phone`, `guest`, `date`, `time`, `message`, `created_at`, `updated_at`) VALUES
(5, 'Edilyn Rilloma De Guzman', 'edilyndeguzman@yahoo.com', '09664136146', '7', '13.06.2022', '09:00', 'See You Soon!', '2022-06-07 00:28:07', '2022-06-07 00:28:07'),
(6, 'Mark Philip Poquiz', 'philippoquiz@yahoo.com', '09291245795', '2', '14.06.2022', '11:00', 'Thank You!', '2022-06-07 02:32:49', '2022-06-07 02:32:49'),
(7, 'Maki De Guzman', 'makideguzman@gmail.com', '09886574326', '6', '30.06.2022', '17:00', 'Thank You!', '2022-06-16 05:44:48', '2022-06-16 05:44:48'),
(8, 'Kathleen Biagtan', 'Kathleen@gmail.com', '09223354678', '3', '03.07.2022', '15:00', 'I\'m excited!', '2022-06-16 05:48:45', '2022-06-16 05:48:45'),
(9, 'May Berlin Carpio', 'mayberlin@gmail.com', '09223354678', '2', '05.07.2022', '10:00', 'Have a nice day.', '2022-06-16 05:49:55', '2022-06-16 05:49:55'),
(10, 'Maria Angela Tamondong', 'maria@gmail.com', '09223354678', '9', '13.07.2022', '00:00', 'Thank you in advance.', '2022-06-16 05:51:20', '2022-06-16 05:51:20');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('5Vz6uEu9B8m7coUzMMpjtNqmZWxEzoqwRQKIzMUe', 4, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.5005.124 Safari/537.36 Edg/102.0.1245.44', 'YTo0OntzOjY6Il90b2tlbiI7czo0MDoiTVh2Nm9GMFZLQmJiUHpMaU83WHdGUzRYc0JnMjdPNHFaU1kxOUdZSSI7czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjU6Imh0dHA6Ly9sb2NhbGhvc3Qvdmlld2NoZWYiO31zOjUwOiJsb2dpbl93ZWJfNTliYTM2YWRkYzJiMmY5NDAxNTgwZjAxNGM3ZjU4ZWE0ZTMwOTg5ZCI7aTo0O30=', 1655624377);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `usertype` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `two_factor_secret` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `two_factor_recovery_codes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `two_factor_confirmed_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_team_id` bigint(20) UNSIGNED DEFAULT NULL,
  `profile_photo_path` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `usertype`, `email_verified_at`, `password`, `two_factor_secret`, `two_factor_recovery_codes`, `two_factor_confirmed_at`, `remember_token`, `current_team_id`, `profile_photo_path`, `created_at`, `updated_at`) VALUES
(4, 'admin', 'admin@gmail.com', '1', NULL, '$2y$10$NANSN4kEEaSc.7/aUy/g8.8Pq9bbRI/pQfUoklIRD/Y7pAKQsgfEy', NULL, NULL, NULL, '7esNj1lWkS13IRSB9FqVkCeUFYjd0MrkCQjWGcG5uEreruNw0ZOtNvTgb8sA', NULL, NULL, '2022-04-18 04:32:26', '2022-04-18 04:32:26'),
(8, 'Edilyn Rilloma De Guzman', 'edilyndeguzman@yahoo.com', '0', NULL, '$2y$10$UKTgUt8MwkvG4svFFEf14u2T/3TWdUHNJtKxmq3kcW7T.p1f2FMly', NULL, NULL, NULL, NULL, NULL, NULL, '2022-06-06 02:36:24', '2022-06-06 02:36:24'),
(9, 'Mark Philip Poquiz', 'philippoquiz@yahoo.com', '0', NULL, '$2y$10$ICkM82QTxENUJ.CV4x9Ib.zv05zM0E3h5vLrI.0GiG7nBxrWF5CYC', NULL, NULL, NULL, NULL, NULL, NULL, '2022-06-07 02:31:59', '2022-06-07 02:31:59'),
(10, 'admin1', 'admin1@gmail.com', '1', NULL, '$2y$10$Y9rw3od1bA/bbIEsaEIXT.DIYJaZRqjErx7JDjgb6.O6PGO4Ri9GC', NULL, NULL, NULL, NULL, NULL, NULL, '2022-06-14 22:20:49', '2022-06-14 22:20:49'),
(13, 'Maki De Guzman', 'makideguzman@gmail.com', '0', NULL, '$2y$10$Gm.q2MvOWRvwo4fXsicsG./aik2Loalw35/4n5qqq4zR8Mo5RjmN.', NULL, NULL, NULL, NULL, NULL, NULL, '2022-06-15 22:53:43', '2022-06-15 22:53:43'),
(14, 'Kathleen Biagtan', 'Kathleen@gmail.com', '0', NULL, '$2y$10$VrH68LrCqWDf38icReFTI.c.pNhOt9rQ0BnhxeItmumnIYyB/lz62', NULL, NULL, NULL, NULL, NULL, NULL, '2022-06-15 22:54:06', '2022-06-15 22:54:06'),
(15, 'May Berlin Carpio', 'mayberlin@gmail.com', '0', NULL, '$2y$10$xncRkKb9WlKa5WW8Kei4DeeQNxoZl6ynnl6I7DT2bmjuc5uYpAwNe', NULL, NULL, NULL, NULL, NULL, NULL, '2022-06-15 22:54:33', '2022-06-15 22:54:33'),
(16, 'Maria Angela Tamondong', 'maria@gmail.com', '0', NULL, '$2y$10$2IsGd2VfvEbAFsMzEJkExeT2jfiXpU77LFKvBx1vXY7Hf0pay5cMy', NULL, NULL, NULL, NULL, NULL, NULL, '2022-06-15 22:55:07', '2022-06-15 22:55:07'),
(17, 'Rina Catalan', 'rinacatalan@gmail.com', '0', NULL, '$2y$10$n5yfB1dhCDU5X9djzQLcReDAHlZx32/1KNYSQU1VdbhFMCZSqWOpq', NULL, NULL, NULL, NULL, NULL, NULL, '2022-06-15 22:55:51', '2022-06-15 22:55:51'),
(18, 'Jhovelyn Dela Vega', 'jhov@gmail.com', '0', NULL, '$2y$10$TlumpuYjoU6h.gH1PhGQ1enElZ3E1sQD1LJKVcFOrlmoAdBPsS9yG', NULL, NULL, NULL, NULL, NULL, NULL, '2022-06-15 22:56:29', '2022-06-15 22:56:29'),
(19, 'Joshua Exiomo', 'joshuaexiomo@gmail.com', '0', NULL, '$2y$10$h9C/y2otjjhV6Alvrefbp.iy6gcNk4a0CVTe09F6RzbC3ylLpbHaK', NULL, NULL, NULL, NULL, NULL, NULL, '2022-06-15 22:57:41', '2022-06-15 22:57:41'),
(20, 'Rowela', 'rowela@gmail.com', '0', NULL, '$2y$10$.RHubGEKHW99vb6OEULRYuHHuabyjV0Slql/yd9nH/ZfhgMPt2RKa', NULL, NULL, NULL, NULL, NULL, NULL, '2022-06-16 02:25:54', '2022-06-16 02:25:54'),
(21, 'Mark Philip Poquiz', 'philip@yahoo.com', '0', NULL, '$2y$10$aZfHy22ALKrscAmtydfwpes31jMrg7rFeX/5up2q4H6/1v8USeyeO', NULL, NULL, NULL, NULL, NULL, NULL, '2022-06-16 19:15:21', '2022-06-16 19:15:21'),
(22, 'Ezza De Guzman', 'ezzadeguzman@yahoo.com', '0', NULL, '$2y$10$c44Ha1YJqt4EZ77Oq2QpWuhGQRVzisfrFphLZeSJMUx.Zj3PbhNqW', NULL, NULL, NULL, NULL, NULL, NULL, '2022-06-18 19:33:37', '2022-06-18 19:33:37');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `abouts`
--
ALTER TABLE `abouts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `food`
--
ALTER TABLE `food`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `foodchefs`
--
ALTER TABLE `foodchefs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `landingpages`
--
ALTER TABLE `landingpages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `reservations`
--
ALTER TABLE `reservations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sessions_user_id_index` (`user_id`),
  ADD KEY `sessions_last_activity_index` (`last_activity`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `abouts`
--
ALTER TABLE `abouts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `food`
--
ALTER TABLE `food`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `foodchefs`
--
ALTER TABLE `foodchefs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `landingpages`
--
ALTER TABLE `landingpages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reservations`
--
ALTER TABLE `reservations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
