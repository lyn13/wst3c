<!-- ***** About Area Starts ***** -->
    
    <section class="section" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-xs-12">
                    <div class="left-text-content">
                        <div class="section-heading">
                            <h6>About Us</h6>
                            <h2>We Leave A Delicious Memory For You</h2>
                        </div>

                        @foreach($data3 as $data3)

                        <p class='description1' style="color: black;">{{$data3->description1}}</p>

                        <p class='description2' style="color: black;">{{$data3->description2}}</p>

                        <p class='description3' style="color: black;">{{$data3->description3}}</p>
                        <div class="row">
                            <div class="col-4">
                                <img src="/aboutimage/{{$data3->image}}" alt="" width="100" height="200px">
                            </div>
                            <div class="col-4">
                                <img  src="/aboutimage/{{$data3->image}}"  class="rounded-circle" alt="Rounded Image" width="100" height="200px">
                            </div>
                            <div class="col-4">
                                <img src="/aboutimage/{{$data3->image}}" alt="" width="100" height="200px">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-xs-12">
                    <div class="right-content">
                        <div class="thumb">
                            <video style="width:570px;" height="570px;" border="solid #04AA6D;" muted autoplay loop>
                            <source src="assets/videos/video.mp4" type="video/mp4" />
                            </video>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
    <!-- ***** About Area Ends ***** -->

