<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Dancing+Script:wght@400;500;600;700&display=swap" rel="stylesheet">

    <title>Hangry Boss Content Management System</title>
<!--
    
TemplateMo 558 Klassy Cafe

https://templatemo.com/tm-558-klassy-cafe

-->
    <!-- Additional CSS Files -->
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">

    <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.css">

     <link rel="stylesheet" href="assets/css/hangryboss.css">

    <link rel="stylesheet" href="assets/css/owl-carousel.css">

    <link rel="stylesheet" href="assets/css/lightbox.css">

    </head>
    <style>
        .logo{
        margin-left: 5px;
        }
         footer .left-text-content p {
        color: #fff;
        font-size: 17px;
        text-align: right;
        }
    </style>
    
    <body>
    
    <!-- ***** Preloader Start ***** -->
    <div id="preloader">
        <div class="jumper">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>  
    <!-- ***** Preloader End ***** -->
    
    
    <!-- ***** Header Area Start ***** -->
    <header class="header-area header-sticky ">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <nav class="main-nav">
                        <!-- ***** Logo Start ***** -->
                        <a href="index.html" class="logo">
                            <img src="assets/images/hangryboss-logo.png" align="Hangry Boss html template" width="120px" height="120px">
                        </a>
                        <!-- ***** Logo End ***** -->
                        <!-- ***** Menu Start ***** -->
                        <ul class="nav">
                            <li class="scroll-to-section"><a href="#top" class="active font-weight-bold">HOME</a></li>
                            <li class="scroll-to-section"><a href="#menu" class="font-weight-bold">PRODUCTS</a></li>

                            <li class="scroll-to-section font-weight-bold">
                                @auth

                        <a href="{{url('/showcart',Auth::user()->id)}}">

                            TRANSACTION{{$count}}
                        </a>

                            @endauth

                            @guest


                            @endguest

                            </a></li> 

                            <li>
                            @if (Route::has('login'))
                                <div class="hidden fixed top-0 right-0 px-6 py-4 sm:block">
                            @auth
                                 <li><x-app-layout>
    
                                 </x-app-layout>
                            </li>
                            @else
                                 <li><a href="{{ route('login') }}" class="text-sm text-gray-700 dark:text-gray-500 underline font-weight-bold">LOGIN</a></li>

                            @if (Route::has('register'))
                            <li><a href="{{ route('register') }}" class="ml-1 text-sm text-gray-700 dark:text-gray-500 underline font-weight-bold">REGISTER</a></li>
                            @endif
                            @endauth
                            </div>
                            @endif   


                            </li>
                        
                        <!-- ***** Menu End ***** -->
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- ***** Header Area End ***** -->

    <!-- ***** Main Banner Area Start ***** -->
   @include("landingpage")
    <!-- ***** Main Banner Area End ***** -->
    
    @include("food")
    
    <!-- ***** Chefs Area Starts ***** -->
    <!-- ***** Chefs Area Ends ***** -->

    <!-- ***** Reservation Us Area Starts ***** -->      
    <!-- ***** Reservation Area Ends ***** -->
    
    <!-- ***** Menu Area Starts ***** -->
    <!-- ***** Chefs Area Ends ***** --> 
    
    <!-- ***** Footer Start ***** -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-xs-12">
                    <div class="right-text-content">
                            <ul class="social-icons">
                                <li><a href="https://web.facebook.com/profile.php?id=100063927280174"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="https://twitter.com/ameliaflorvi"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="https://instagram.com/ameliaflorvi?igshid=YmMyMTA2M2Y="><i class="fa fa-instagram"></i></a></li>
                            </ul>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="logo">
                        <a href="index.html"><img src="assets/images/hangryboss-logo.png" alt="" width="100px" height="100px" ></a>
                    </div>
                </div>
                <div class="col-lg-4 col-xs-12">
                    <div class="left-text-content">
                       <p>© 2022 Hangry Boss
                        <br>All Right Reserved.
                        <br>Created by:&nbsp&nbsp&nbsp&nbsp&nbsp 
                        <br>Edilyn De Guzman.</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="assets/js/jquery-2.1.0.min.js"></script>

    <!-- Bootstrap -->
    <script src="assets/js/popper.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- Plugins -->
    <script src="assets/js/owl-carousel.js"></script>
    <script src="assets/js/accordions.js"></script>
    <script src="assets/js/datepicker.js"></script>
    <script src="assets/js/scrollreveal.min.js"></script>
    <script src="assets/js/waypoints.min.js"></script>
    <script src="assets/js/jquery.counterup.min.js"></script>
    <script src="assets/js/imgfix.min.js"></script> 
    <script src="assets/js/slick.js"></script> 
    <script src="assets/js/lightbox.js"></script> 
    <script src="assets/js/isotope.js"></script> 
    
    <!-- Global Init -->
    <script src="assets/js/custom.js"></script>
    <script>

        $(function() {
            var selectedClass = "";
            $("p").click(function(){
            selectedClass = $(this).attr("data-rel");
            $("#portfolio").fadeTo(50, 0.1);
                $("#portfolio div").not("."+selectedClass).fadeOut();
            setTimeout(function() {
              $("."+selectedClass).fadeIn();
              $("#portfolio").fadeTo(50, 1);
            }, 500);
                
            });
        });

        

    </script>
  </body>
</html>