<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Hangry Boss Admin</title>
        <x-app-layout>

        </x-app-layout>
         @include("admin.admincss")
  </head>
  <style>
  table {
  border-collapse: collapse;
  width: 100%;
  }

  th, td {
  text-align: center;
  padding: 8px;
  border: 1px solid #04AA6D;
  }


  th {
  background-color: #04AA6D;
  color: white;
  }
  </style>
  <body>
  	<div class="container-scroller">
   
    @include("admin.navbar")

    <div style="position: relative; top: 5px; left: 10px;">

    	<table>
    		<tr>
    			<th style="padding: 30px">Name</th>
    			<th style="padding: 30px">Email</th>
    			<th style="padding: 30px">Action</th>
    		</tr>
    		@foreach($data as $data)

    		<tr align="center" bgcolor="#f2f2f2">
    			<td style="color:black">{{$data->name}}</td>
    			<td style="color:black">{{$data->email}}</td>

    			@if($data->usertype=="0")
    			<td><a href="{{url('/deleteuser', $data->id)}}">Delete</a></td>
    			@else
    			<td style="color:black">Not Allowed</a></td>

    			@endif
    		</tr>
    		@endforeach

    	</table>
   </div>
</div>
    @include("admin.adminscript")
</body>
</html>
