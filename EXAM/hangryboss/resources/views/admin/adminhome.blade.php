<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Hangry Boss Admin</title>
    <x-app-layout>

    </x-app-layout>
    @include("admin.admincss")
  </head>
  <body>

    @include("admin.navbar")


    @include("admin.adminscript")

  </body>
</html>