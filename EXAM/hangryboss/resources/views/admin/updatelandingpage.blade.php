<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Hangry Boss Admin</title>
    <x-app-layout>

    </x-app-layout>
  <base href="/public">
  @include("admin.admincss")
  </head>
  <style>
  label {
  color: black;
  }

  input[type=text] {
  width: 100%;
  padding: 12px 20px;
  margin: 4px 0;
  display: inline-block;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
  height: 15px;
  color: black;
  }

  input[type=submit] {
  width: 50%;
  height: 35px;
  background-color: #04AA6D;
  color: white;
  padding: 5px 5px;
  margin: 8px 0;
  border: none;
  border-radius: 4px;
  cursor: pointer;
  margin-left: 110px;
  }

  .container {
  border-radius: 5px;
  background-color: #f2f2f2;
  padding: 20px;
  height: 420px;
  width: 380px;
 }

  </style>
  <body>
  	<div class="container-scroller">
    @include("admin.navbar")
    
    	<div class="container" style="margin-top: 5px; margin-right: 400px;" >
      <form action="{{url('/updateimagelandingpage',$data->id)}}" method="post" enctype="multipart/form-data">

       @csrf

        <div class="row">
          <div class="col-10">
    			<label for="image">Old Image</label>
          </div>
          <div class="col-12">
    			<center><img height="150" width="200" src="/landingpageimage/{{$data->image}}"></center>
          </div>
    		</div>

    		<div class="row">
          <div class="col-10">
    			<label for="image">New Image</label>
          </div>
          <div class="col-12">
    			<input style="color: black" type="file" name="image" required>
          </div>
    		</div>


    		<div class="row">
    			 <input type="submit" value="Update Home Picture">
    		</div>
    	</form>
    </div>
    </div>
    <div>
	</div>
    @include("admin.adminscript")

  </body>
</html>