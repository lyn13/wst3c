<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Hangry Boss Admin</title>
    <x-app-layout>

    </x-app-layout>
    @include("admin.admincss")
  </head>
  <style>
   label {
  color: black;
  }

  input[type=text] {
  width: 100%;
  padding: 12px 20px;
  margin: 4px 0;
  display: inline-block;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
  height: 15px;
  color: black;
  }

  input[type=submit] {
  width: 35%;
  height: 35px;
  background-color: #04AA6D;
  color: white;
  padding: 5px 5px;
  margin: 8px 0;
  border: none;
  border-radius: 4px;
  cursor: pointer;
  margin-left: 110px;
  }

  table {
  border-collapse: collapse;
  width: 100%;
  }

  th, td {
  text-align: center;
  padding: 8px;
  border: 1px solid #04AA6D;
  }

  th {
  background-color: #04AA6D;
  color: white;
  }

 .container {
  border-radius: 5px;
  background-color: #f2f2f2;
  padding: 20px;
  height: 180px;
  width: 350px;
  }

  </style>
  <body>
    <div class="container-scroller">
    @include("admin.navbar")

    <div class="container" style="margin-top: 5px; margin-right: 5px; margin-left: 0px;" >
    <form action="{{url('/uploadlandingimage')}}" method="post" enctype="multipart/form-data">
      @csrf
      
       
      <div class="row">
      <div class="col-10">
      <label for="image">Select Image:</label>
      </div>
      <div class="col-12">
      <input style="color: black"type="file" name="image" required><br><br>
      </div>
      </div>

      <div class="row">
      <input type="submit" value="Save">
      </div>

     </form>
     </div>

    <div>
      <table style="margin-top: 5px;  margin-right:5px; margin-bottom: 10px">
        <tr>
          <th style="padding: 30px">Image</th>
          <th style="padding: 30px">Action</th>
        </tr>
       
        @foreach($data4 as $data4)

        <tr align="center" bgcolor="#f2f2f2">
          <td><center><img height="150" width="200" src="/landingpageimage/{{$data4->image}}"></center></td>
          <td><a href="{{url('/deletelandingpage',$data4->id)}}">Delete</a> <a href="{{url('/updatelandingpage',$data4->id)}}">Update</a></td>
        </tr> 
         @endforeach 
           

    @include("admin.adminscript")

  </body>
</html>

