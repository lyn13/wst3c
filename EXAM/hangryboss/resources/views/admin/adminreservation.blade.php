<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Hangry Boss Admin</title>
    <x-app-layout>

    </x-app-layout>
    @include("admin.admincss")
</head>
<style>
table {
  border-collapse: collapse;
  width: 100%;
}

th, td {
  text-align: center;
  padding: 8px;
  border: 1px solid #04AA6D;
}


th {
  background-color: #04AA6D;
  color: white;
}

</style>
  <body>
  	<div class="container-scroller">

    @include("admin.navbar")

    <div style="position: relative; top: 5px;">
    	
    	<table style="margin-bottom: 20px;" >
    		<tr>
    			<th style="padding: 20px;">Name</th>
    			<th style="padding: 25px;">Email Address</th>
    			<th style="padding: 15px;">Phone Number</th>
    			<th style="padding: 3px;">Guest</th>
    			<th style="padding: 30px;">Date</th>
    			<th style="padding: 12px;">Time</th>
    			<th style="padding: 15px;">Message</th>
          <th style="padding: 15px;">Action</th>
          <th style="padding: 20px;">Status</th>
    	    </tr>

    			@foreach($data as $data)


    		<tr align="center" bgcolor="#f2f2f2">
    			<td style="color:black">{{$data->name}}</td>
    			<td style="color:black">{{$data->email}}</td>
    			<td style="color:black">{{$data->phone}}</td>
    			<td style="color:black">{{$data->guest}}</td>
    			<td style="color:black">{{$data->date}}</td>
    			<td style="color:black">{{$data->time}}</td>
    			<td style="color:black">{{$data->message}}</td>
          <td><a href="{{url('/deletereservation',$data->id)}}">Delete</a></td> 
          <td style="color:#04AA6D">Approved</td> 
        </tr>
    			   @endforeach

    	</table>
    </div>
</div>

    @include("admin.adminscript")

  </body>
</html>