<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Hangry Boss Admin</title>
    <x-app-layout>

    </x-app-layout>
    @include("admin.admincss")
</head>
<style>
table {
  border-collapse: collapse;
  width: 100%;
}

th, td {
  text-align: center;
  padding: 8px;
  border: 1px solid #04AA6D;
}

th {
  background-color: #04AA6D;
  color: white;
}

input[type=submit] {
  width: 10%;
  height: 35px;
  background-color: #04AA6D;
  color: white;
  padding: 5px 5px;
  margin: 8px 0;
  border: none;
  border-radius: 4px;
  cursor: pointer;
  }

</style>
  <body>
  	<div class="container-scroller">

    @include("admin.navbar")

    <div class="container" style="position: relative; top: 5px; margin-left: 30px;">

    <form action="{{url('/search')}}" method="get">

    	@csrf
      <div class="col-12">
    	<input type="text" name="search" placeholder="Enter search" style="color: blue">
    	<input type="submit" value="Search" class="btn btn-success">
      </div>

    </form>

    <div style="position: relative; top: 5px; margin-bottom: 20px; margin-left: 0px;">
    	
    	<table >
    		<tr>
          <th style="padding: 35px;">Product ID</th>
          <th style="padding: 25px;">Delivery Date</th>
          <th style="padding: 20px;">Delivery Time</th>
    			<th style="padding: 20px;">Foodname</th>
    			<th style="padding: 25px;">Price</th>
    			<th style="padding: 5px;">Quantity</th>
    			<th style="padding: 15px;">Total Price</th>
          <th style="padding: 15px;">Action</th>
    	    </tr>

    			@foreach($data as $data)


    		<tr align="center" bgcolor="#f2f2f2">
          <td style="color:black">{{$data->id}}</td>
          <td style="color:black">{{$data->date}}</td>
          <td style="color:black">{{$data->time}}</td>
    			<td style="color:black">{{$data->foodname}}</td>
    			<td style="color:black">{{$data->price}}</td>
    			<td style="color:black">{{$data->quantity}}</td>
    			<td style="color:black">{{$data->price * $data->quantity}}</td>
          <td ><a href="{{url('/deleteorder',$data->id)}}">Delete</a></td> 
           
            </tr>
    			   @endforeach

    	</table>
    </div>
	
	</div>

    @include("admin.adminscript")

  </body>
</html>