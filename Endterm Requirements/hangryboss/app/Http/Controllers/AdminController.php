<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;

use App\Models\Food;

use App\Models\Reservation;

use App\Models\Foodchef;

use App\Models\Order;

use App\Models\About;

use App\Models\Landingpage;



use Illuminate\Support\Facades\Auth;


class AdminController extends Controller
{
    public function user()
    {
    	$data=user::all();
    	return view("admin.users",compact("data"));
    }

    public function landingpage()
    {
        $data4=landingpage::all();
        return view("admin.landingpage",compact("data4"));
    }

      public function uploadlandingimage(Request $request)
    {
        $data = new landingpage;

        
    $image=$request->image;

    $imagename =time().'.'.$image->getClientOriginalExtension();

            $request->image->move('landingpageimage',$imagename);

            $data->image=$imagename;

            $data->save();

            return redirect()->back();

    }

     public function deletelandingpage($id)
    {
        $data=landingpage::find($id);
        $data->delete();
        return redirect()->back();
    }

     public function updatelandingpage($id)
    {
        $data=landingpage::find($id);
        return view("admin.updatelandingpage",compact("data"));
    }

     public function updateimagelandingpage(Request $request, $id)
    {
        $data=landingpage::find($id);

         $image=$request->image;


    $imagename =time().'.'.$image->getClientOriginalExtension();

            $request->image->move('landingpageimage',$imagename);

            $data->image=$imagename;

            $data->save();

            return redirect()->back();
        
    }

    
    public function about()
    {
        $data3=about::all();
        return view("admin.about",compact("data3"));
    }

      public function uploadabout(Request $request)
    {
        $data = new about;

        
    $image=$request->image;

    $imagename =time().'.'.$image->getClientOriginalExtension();

            $request->image->move('aboutimage',$imagename);

            $data->image=$imagename;

            $data->description1=$request->description1;

            $data->description2=$request->description2;

            $data->description3=$request->description3;

            $data->save();

            return redirect()->back();

    }

     public function deleteabout($id)
    {
        $data=about::find($id);
        $data->delete();
        return redirect()->back();
    }

    public function updateabout($id)
    {
        $data=about::find($id);
        return view("admin.updateabout",compact("data"));
    }

    public function updatehbabout(Request $request, $id)
    {
        $data=about::find($id);

         $image=$request->image;


    $imagename =time().'.'.$image->getClientOriginalExtension();

            $request->image->move('aboutimage',$imagename);

            $data->image=$imagename;


            $data->description1=$request->description1;

            $data->description2=$request->description2;

            $data->description3=$request->description3;

            $data->save();

            return redirect()->back();
        
    }


    public function deleteuser($id)
    {
    	$data=user::find($id);
    	$data->delete();
    	return redirect()->back();
    }

    public function deletemenu($id)
    {
        $data=food::find($id);
        $data->delete();
        return redirect()->back();
    }


    public function foodmenu()
    {
        $data=food::all();
        return view("admin.foodmenu",compact("data"));
    }

    public function updateview($id)
    {
        $data=food::find($id);
        return view("admin.updateview",compact("data"));
    }

    public function update(Request $request, $id)
    {
        $data=food::find($id);

         $image=$request->image;


    $imagename =time().'.'.$image->getClientOriginalExtension();

            $request->image->move('foodimage',$imagename);

            $data->image=$imagename;


            $data->title=$request->title;

            $data->price=$request->price;

            $data->description=$request->description;

            $data->save();

            return redirect()->back();
        
    }



    public function upload(Request $request)
    {
        $data = new food;

        
    $image=$request->image;


    $imagename =time().'.'.$image->getClientOriginalExtension();

            $request->image->move('foodimage',$imagename);

            $data->image=$imagename;


            $data->title=$request->title;

            $data->price=$request->price;

            $data->description=$request->description;

            $data->save();

            return redirect()->back();

    }


     public function reservation(Request $request)
    {
        $data = new reservation;


            $data->name=$request->name;

            $data->email=$request->email;

            $data->phone=$request->phone;

            $data->guest=$request->guest;

            $data->date=$request->date;

            $data->time=$request->time;

            $data->message=$request->message;

            $data->save();

            return redirect()->back();

    }

    public function viewreservation()
    {

     if(Auth::id())   
     {

        $data=reservation::all();

        return view("admin.adminreservation",compact("data"));   
     }

        else
        {

           return redirect('login'); 

        }
    }

      public function deletereservation($id)
    {
        $data=reservation::find($id);
        $data->delete();
        return redirect()->back();
    }


    public function viewchef()
    {
        $data=foodchef::all();
        return view("admin.adminchef",compact("data"));   
    }


    public function uploadchef(Request $request)
    {
        $data = new foodchef;
        
         $image=$request->image;

          $imagename =time().'.'.$image->getClientOriginalExtension();

            $request->image->move('chefimage',$imagename);

            $data->image=$imagename;



            $data->name=$request->name;

            $data->speciality=$request->speciality;

            $data->save();

            return redirect()->back();
            
    }

    public function updatechef($id)
    {
        $data=foodchef::find($id);
        return view("admin.updatechef",compact("data"));
    }


     public function updatefoodchef(Request $request,$id)
    {
        $data=foodchef::find($id);

         $image=$request->image;

         
          if ($image){

          $imagename =time().'.'.$image->getClientOriginalExtension();

            $request->image->move('chefimage',$imagename);

            $data->image=$imagename;
        }


            $data->name=$request->name;

            $data->speciality=$request->speciality;

            $data->save();

            return redirect()->back();
        
    }


     public function deletechef($id)
    {
        $data=foodchef::find($id);
        $data->delete();
        return redirect()->back();
    }


     public function orders()
    {
        $data=order::all();

        return view('admin.orders',compact('data'));
    }

    public function deleteorder($id)
    {
        $data=order::find($id);
        $data->delete();
        return redirect()->back();
    }


    public function search(Request $request)
    {
        $search=$request->search;

        $data=order::where('name','Like','%'.$search.'%')->orWhere('foodname','Like','%'.$search.'%')->get();

        return view('admin.orders',compact('data'));
    }


}
