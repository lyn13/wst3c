<style>
input[type=number] {
  width: 100%;
  padding: 12px 20px;
  margin: 4px 0;
  display: inline-block;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
  height: 15px;
  color: black;
  }

  input[type=submit] {
  width: 50%;
  height: 35px;
  background-color: #04AA6D;
  color: white;
  border: none;
  border-radius: 4px;
  cursor: pointer;
  }

   input[type=number] {
  width: 40%;
  display: inline-block;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
  height: 15px;
  color: black;
  margin-left: 1px;
  }
  p{
    color: white;
  }

</style>

<!-- ***** Menu Area Starts ***** -->
    <section class="section" id="menu">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <div class="section-heading">
                        <h6>Our Menu</h6>
                        <h2>Our Selection of Foods with Quality Taste</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="menu-item-carousel">
            <div class="col-lg-12">
                <div class="owl-menu-item owl-carousel">

                  @foreach($data as $data)
                  <form action="{{url('/addcart',$data->id)}}" method="post">

                   @csrf

                    <div class="item">
                        <div style="background-image: url('/foodimage/{{$data->image}}');" class='card'>
                            <div class="price"><h6>{{$data->price}}</h6></div>
                            <div class='info'>
                              <h1 class='title'>{{$data->title}}</h1>
                              <p class='description' style="color:white;">{{$data->description}}</p>
                              <div class="main-text-button">
                                  <div class="scroll-to-section"><a href="#reservation">Make Reservation <i class="fa fa-angle-down"></i></a></div>
                              </div>
                            </div>
                        </div>
                        <center>
                        <input type="number" name="quantity" min="1" value="1">
                        <input type="submit" value="Add Cart">
                        </center>
                    </div>
                    </form>

                    @endforeach
         
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Menu Area Ends ***** -->


