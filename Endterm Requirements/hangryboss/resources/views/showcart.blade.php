<!DOCTYPE html>
<html lang="en">

  <head>
    <base href="/publics">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Dancing+Script:wght@400;500;600;700&display=swap" rel="stylesheet">


    <title>Hangry Boss Content Management System</title>
<!--
    
TemplateMo 558 Klassy Cafe

https://templatemo.com/tm-558-klassy-cafe

-->
    <!-- Additional CSS Files -->
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">

    <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.css">

    <link rel="stylesheet" href="assets/css/hangryboss.css">

    <link rel="stylesheet" href="assets/css/owl-carousel.css">

    <link rel="stylesheet" href="assets/css/lightbox.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

    </head>
    <style>
        th{
          color: white;
          text-align: center;
          background-color: #04AA6D;
        }

        table{
            width: 50%;
        }

        td{
          color: white;  
        }

        .jumbotron{
         height: 690px;
         width: 500px;
         background-color: #04AA6D;
        }

        h1{
            font-family: cursive;
            color: white;
        }

        body{
           background-color: #white;
        }

        p{
            color: white;
        }

        label{
            color: white;
        }

        .logo{
        margin-left: 150px;
        }

        input[type=submit] {
        width: 35%;
        height: 35px;
        background-color: orange;
        color: white;
        padding: 5px 5px;
        margin: 8px 0;
        border: none;
        border-radius: 4px;
        cursor: pointer;
        margin-left: 5px;
        }

        input[type=button] {
        width: 15%;
        height: 35px;
        background-color: blue;
        color: white;
        padding: 5px 5px;
        margin: 8px 0;
        border: none;
        border-radius: 4px;
        cursor: pointer;
        margin-left: 5px;
        }
        .logo{
        margin-left: 5px;
        }
        footer{
            margin-top: 0;
        }
        footer .right-text-content p {
        color: #fff;
        font-size: 12px;
        margin-right: 22px;
        text-transform: uppercase;
        }
         footer .left-text-content p {
        margin-top: 5px;
        color: #fff;
        font-size: 17px;
        text-align: right;
        }


    </style>
    
    <body>
    
    <!-- ***** Preloader Start ***** -->
    <div id="preloader">
        <div class="jumper">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>  
    <!-- ***** Preloader End ***** -->
    
    
    <!-- ***** Header Area Start ***** -->
    <header class="header-area header-sticky">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <nav class="main-nav">
                        <!-- ***** Logo Start ***** -->
                        <a href="index.html" class="logo">
                            <img src="assets/images/hangryboss-logo.png" align="Hangry Boss html template" width="120px" height="120px">
                        </a>
                        <!-- ***** Logo End ***** -->
                        <!-- ***** Menu Start ***** -->
                        <ul class="nav">
                            <li class="scroll-to-section"><a href="#top" class="font-weight-bold">HOME</a></li>
                            <li class="scroll-to-section"><a href="#about" class="font-weight-bold">ABOUT</a></li>
                            <li class="scroll-to-section"><a href="#menu" class="font-weight-bold">MENU</a></li>
                            <li class="scroll-to-section"><a href="#chefs" class="font-weight-bold">CHEFS</a></li> 
                            <li class="scroll-to-section"><a href="#reservation" class="font-weight-bold">CONTACT US</a></li> 

                            <li class="scroll-to-section font-weight-bold">
                                @auth

                        <a href="{{url('/showcart',Auth::user()->id)}}">

                            ORDER( {{$count}} )
                        </a>

                            @endauth

                            @guest

                            CART[0]

                            @endguest

                            </a></li> 

                            <li>
                            @if (Route::has('login'))
                                <div class="hidden fixed top-0 right-0 px-6 py-4 sm:block">
                            @auth
                                 <li><x-app-layout>
    
                                 </x-app-layout>
                            </li>
                            @else
                                 <li><a href="{{ route('login') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Log in</a></li>

                            @if (Route::has('register'))
                            <li><a href="{{ route('register') }}" class="ml-4 text-sm text-gray-700 dark:text-gray-500 underline">Register</a></li>
                            @endif
                            @endauth
                            </div>
                            @endif   


                            </li>
                        
                        <!-- ***** Menu End ***** -->
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <br><br>
    <!-- ***** Header Area End ***** -->

    <div id="top">
    <table align="center" bgcolor="#199164">
        <tr>
            <th style="padding: 30px;">FOOD NAME</th>
            <th style="padding: 30px;">PRICE</th>
            <th style="padding: 30px;">QUANTITY</th>
            <th style="padding: 30px;">ACTION</th>
        </tr>

        <form action="{{url('orderconfirm')}}" method="POST">

            @csrf

        @foreach($data as $data)
        
        <tr align="center">
            
            <td style="padding:10px">
            <input type="text" name="foodname[]" value="{{$data->title}}" hidden="">
            {{$data->title}}</td>

            <td>
            <input type="text" name="price[]" value="{{$data->price}}" hidden="">
            {{$data->price}}</td>

            <td>
            <input type="text" name="quantity[]" value="{{$data->quantity}}" hidden="">
            {{$data->quantity}}</td>
        </tr>
            
        @endforeach

       @foreach($data2 as $data2)

       <tr style="position: relative; top: -210px; right: -530px;">
        <td><a href="{{url('/remove',$data2->id)}}" class="btn btn-danger">Remove</a></td>

        </tr>
        @endforeach

    </table>
    <br>
        <div align="center" style="padding: 10px;">
        <input type="button" id="order" value="Order Now">



        
        <div id="appear" align="center" style="padding: 10px; display: none;">
        
        <div class="container">
       <div class="jumbotron">
        <img src="assets/images/hangryboss-logo.png" align="Hangry Boss html template" width="80px" height="80px">     
         <h1><strong>HANGRY BOSS ORDER FORM</strong></h1>
         <p>Please fill out the form completely to ensure the order is filled accurately and on time.</p>
         <br>

        <div class="row">
          <div class="col-5">
            <label for="name"><strong>Name:</strong></label>
          </div>
        <div class="col-12">
          <input type="text" name="name"  placeholder="Your Name" required="">
         </div>
        </div>

        <div class="row">
         <div class="col-5">
           <label for="phone"><strong>Phone:</strong></label>
         </div>
        <div class="col-12">
           <input type="number" name="phone" placeholder="Your Phone Number" required="">
         </div>
        </div>

        <div class="row">
         <div class="col-5">
           <label for="address"><strong>Address:</strong></label>
         </div>
        <div class="col-12">
           <input type="text" name="address" placeholder="Your Address" required="">
         </div>
        </div>

         <div class="row">
         <div class="col-5">
           <label for="date"><strong>Delivery Date:</strong></label>
         </div>
        <div class="col-11 ">
           <input type="date" name="date" required="">
         </div>
        </div>
        

        <div class="row">
         <div class="col-5">
           <label for="time"><strong>Delivery Time:</strong></label>
         </div>
        <div class="col-10">
           <input type="time" name="time" required="">
         </div>
        </div>

        <div style="padding: 10px;">
            <input type="submit"  value="Order Confirm">
            <input type="button"  id="close"  value="Close">
        </div>
    </div>
</div>
</div>
</form>


    </div>
 <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-xs-12 ml-5">
                    <div class="right-text-content">
                            <ul class="social-icons">
                                <li><a href="https://web.facebook.com/profile.php?id=100063927280174"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="https://twitter.com/ameliaflorvi"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="https://instagram.com/ameliaflorvi?igshid=YmMyMTA2M2Y="><i class="fa fa-instagram"></i></a></li>
                            </ul>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="logo">
                        <center><a href="index.html"><img src="assets/images/hangryboss-logo.png" alt="" width="100px" height="100px"  ></a></center>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="left-text-content mr-5">
                        <p>© 2022 Hangry Boss
                        <br>All Right Reserved.
                        <br>Created by:&nbsp&nbsp&nbsp&nbsp&nbsp 
                        <br>Edilyn De Guzman.</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>

   
    <script type="text/javascript">

        $("#order").click(

            function()
            {
                $("#appear").show();
            }

            );

        $("#close").click(

            function()
            {
                $("#appear").hide();
            }

            );
        


    </script>

     <!-- jQuery -->
    <script src="assets/js/jquery-2.1.0.min.js"></script>

    <!-- Bootstrap -->
    <script src="assets/js/popper.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- Plugins -->
    <script src="assets/js/owl-carousel.js"></script>
    <script src="assets/js/accordions.js"></script>
    <script src="assets/js/datepicker.js"></script>
    <script src="assets/js/scrollreveal.min.js"></script>
    <script src="assets/js/waypoints.min.js"></script>
    <script src="assets/js/jquery.counterup.min.js"></script>
    <script src="assets/js/imgfix.min.js"></script> 
    <script src="assets/js/slick.js"></script> 
    <script src="assets/js/lightbox.js"></script> 
    <script src="assets/js/isotope.js"></script> 
    
    <!-- Global Init -->
    <script src="assets/js/custom.js"></script>
    <script>

        $(function() {
            var selectedClass = "";
            $("p").click(function(){
            selectedClass = $(this).attr("data-rel");
            $("#portfolio").fadeTo(50, 0.1);
                $("#portfolio div").not("."+selectedClass).fadeOut();
            setTimeout(function() {
              $("."+selectedClass).fadeIn();
              $("#portfolio").fadeTo(50, 1);
            }, 500);
                
            });
        });

    </script>
  </body>
</html>