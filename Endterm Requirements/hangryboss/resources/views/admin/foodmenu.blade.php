<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Hangry Boss Admin</title>
        <x-app-layout>

        </x-app-layout>

       @include("admin.admincss")
  </head>
  <style>
  label {
  color: black;
  }

  input[type=text] {
  width: 100%;
  padding: 12px 20px;
  margin: 4px 0;
  display: inline-block;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
  height: 15px;
  color: black;
  }

  input[type=number] {
  width: 100%;
  padding: 12px 20px;
  margin: 4px 0;
  display: inline-block;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
  height: 15px;
  color: black;
  }

  input[type=submit] {
  width: 35%;
  height: 35px;
  background-color: #04AA6D;
  color: white;
  padding: 5px 5px;
  margin: 8px 0;
  border: none;
  border-radius: 4px;
  cursor: pointer;
  margin-left: 90px;
  }

  table {
  border-collapse: collapse;
  width: 100%;
}

th, td {
  text-align: center;
  padding: 8px;
  border: 1px solid #04AA6D;
}


th {
  background-color: #04AA6D;
  color: white;
 }


.container {
  border-radius: 5px;
  background-color: #f2f2f2;
  padding: 20px;
  height: 310px;
  width: 300px;
}
  </style>

  <body>
    <div class="container-scroller">
    @include("admin.navbar")

    	<div class="container" style="margin-top: 5px; margin-right: 5px; margin-left: 20px;" >
      <form action="{{url('/uploadfood')}}" method="post" enctype="multipart/form-data">

            @csrf

            <div class="row">
            <div class="col-10">
    		    <label for="title">Title:</label>
            </div>
            <div class="col-12">
            <input type="text" name="title" placeholder=" Enter title" required><br>
            </div>
            </div>

            <div class="row">
            <div class="col-10" >
            <label for="price">Price:</label>
            </div>
            <div class="col-12">
            <input type="number" name="price" placeholder=" Enter price" required><br>
            </div>
            </div>

            <div class="row">
            <div class="col-10">
            <label for="image">Select Image:</label>
            </div>
            <div class="col-4">
            <input style="color: black"type="file" name="image" required><br>
            </div>
            </div>
            
            <div class="row">
            <div class="col-10">
            </div>
            <div class="col-12">
    		    <label for="description">Description:</label><br>
            <input type="text" name="description" placeholder=" Enter description" required><br>
            </div>
            </div>

            <div class="row">
            <input type="submit" value="Save">
            </div>
    		
    	</form>
    </div>

            <div>
                <table style="margin-top: 5px; margin-right: 120px; margin-bottom: 20px;">
                    <tr>
                        <th style="padding: 30px">Food Name</th>
                        <th style="padding: 30px">Price</th>
                        <th style="padding: 30px">Description</th>
                        <th style="padding: 30px">Image</th>
                        <th style="padding: 30px">Action</th>
                    </tr>

                     @foreach($data as $data)

                        <tr align="center" bgcolor="#f2f2f2">
                            <td style="color:black">{{$data->title}}</td>
                            <td style="color:black">{{$data->price}}</td>
                            <td style="color:black">{{$data->description}}</td>
                            <td><center><img height="120" width="120" src="/foodimage/{{$data->image}}"></center></td>
                            <td><a href="{{url('/deletemenu',$data->id)}}">Delete</a>  <a href="{{url('/updateview',$data->id)}}">Update</a></td>
                        </tr>
                     @endforeach
                </table>
            </div>
        </div>
        @include("admin.adminscript")
    </body>
</html>