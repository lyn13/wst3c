<div id="top">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4">
                    <div class="left-content">
                        <div class="inner-content">
                            <h4>Hangry Boss</h4>
                            <h6><center>AN ORDER A DAY
                            <br>KEEPS THE HUNGER 
                            <br>AND ANGER AWAY</center></h6>
                            <div class="main-white-button scroll-to-section">
                                <center><a href="#reservation">Make A Reservation</center></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="main-banner header-text">
                        <div class="Modern-Slider">
                          <!-- Item -->


                        @foreach($data4 as $data4)

                          <div class="item">
                            <div class="img-fill">
                                <img src="/landingpageimage/{{$data4->image}}" alt="" width="100" height="800">
                            </div>
                          </div>
                          @endforeach
                          <!-- // Item -->
                          <!-- // Item -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>