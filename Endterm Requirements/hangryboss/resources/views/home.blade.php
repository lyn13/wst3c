<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Dancing+Script:wght@400;500;600;700&display=swap" rel="stylesheet">

    <title>Hangry Boss Content Management System</title>
<!--
    
TemplateMo 558 Klassy Cafe

https://templatemo.com/tm-558-klassy-cafe

-->
    <!-- Additional CSS Files -->
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">

    <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.css">

     <link rel="stylesheet" href="assets/css/hangryboss.css">

    <link rel="stylesheet" href="assets/css/owl-carousel.css">

    <link rel="stylesheet" href="assets/css/lightbox.css">

    </head>
    <style>
        .logo{
        margin-left: 5px;
        }
         footer .left-text-content p {
        margin-top: 5px;
        color: #fff;
        font-size: 17px;
        text-align: right;
        }
    </style>
    
    <body>
    
    <!-- ***** Preloader Start ***** -->
    <div id="preloader">
        <div class="jumper">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>  
    <!-- ***** Preloader End ***** -->
    
    
    <!-- ***** Header Area Start ***** -->
    <header class="header-area header-sticky ">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <nav class="main-nav">
                        <!-- ***** Logo Start ***** -->
                        <a href="index.html" class="logo">
                            <img src="assets/images/hangryboss-logo.png" align="Hangry Boss html template" width="120px" height="120px">
                        </a>
                        <!-- ***** Logo End ***** -->
                        <!-- ***** Menu Start ***** -->
                        <ul class="nav">
                            <li class="scroll-to-section"><a href="#top" class="active font-weight-bold">HOME</a></li>
                            <li class="scroll-to-section"><a href="#about" class="font-weight-bold">ABOUT</a></li>
                            <li class="scroll-to-section"><a href="#menu" class="font-weight-bold">MENU</a></li>
                            <li class="scroll-to-section"><a href="#chefs" class="font-weight-bold">CHEFS</a></li> 
                            <li class="scroll-to-section"><a href="#reservation" class="font-weight-bold">CONTACT US</a></li> 

                            <li class="scroll-to-section font-weight-bold">
                                @auth

                        <a href="{{url('/showcart',Auth::user()->id)}}">

                            ORDER{{$count}}
                        </a>

                            @endauth

                            @guest


                            @endguest

                            </a></li> 

                            <li>
                            @if (Route::has('login'))
                                <div class="hidden fixed top-0 right-0 px-6 py-4 sm:block">
                            @auth
                                 <li><x-app-layout>
    
                                 </x-app-layout>
                            </li>
                            @else
                                 <li><a href="{{ route('login') }}" class="text-sm text-gray-700 dark:text-gray-500 underline font-weight-bold">LOGIN</a></li>

                            @if (Route::has('register'))
                            <li><a href="{{ route('register') }}" class="ml-1 text-sm text-gray-700 dark:text-gray-500 underline font-weight-bold">REGISTER</a></li>
                            @endif
                            @endauth
                            </div>
                            @endif   


                            </li>
                        
                        <!-- ***** Menu End ***** -->
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- ***** Header Area End ***** -->

    <!-- ***** Main Banner Area Start ***** -->
   @include("landingpage")
    <!-- ***** Main Banner Area End ***** -->
    @include("about")
    
    @include("food")
    
    <!-- ***** Chefs Area Starts ***** -->
    @include("foodchef")
    <!-- ***** Chefs Area Ends ***** -->

    <!-- ***** Reservation Us Area Starts ***** -->
    @include("reservation")        
    <!-- ***** Reservation Area Ends ***** -->
    
    <!-- ***** Menu Area Starts ***** -->
    <section class="section" id="offers">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 offset-lg-4 text-center">
                    <div class="section-heading">
                        <h6>Hangry Boss</h6>
                        <h2>Hangry Boss Special Menu Offers</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="row" id="tabs">
                        <div class="col-lg-12">
                            <div class="heading-tabs">
                                <div class="row">
                                    <div class="col-sm-10 offset-lg-1">
                                        <ul>
                                          <li><a href='#tabs-1'><img src="assets/images/tab-icon-01.jpg" alt="">Wings</a></li>
                                          <li><a href='#tabs-2'><img src="assets/images/tab-icon-02.jpg" alt="">Ramen</a></a></li>
                                          <li><a href='#tabs-3'><img src="assets/images/tab-icon-03.jpg" alt="">Pizza</a></a></li>
                                          <li><a href='#tabs-4'><img src="assets/images/tab-icon-04.jpg" alt="">Hotdog Buns</a></a></li>
                                          <li><a href='#tabs-5'><img src="assets/images/tab-icon-05.jpg" alt="">Nachos</a></a></li>
                                          <li><a href='#tabs-6'><img src="assets/images/tab-icon-06.jpg" alt="">Milktea</a></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <section class='tabs-content'>
                                <article id='tabs-1'>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="row">
                                                <div class="left-list">
                                                    <div class="col-lg-12">
                                                        <div class="tab-item">
                                                            <img src="assets/images/tab-item-classicbuffalo.jpg" alt="">
                                                            <h4>Classic Buffalo</h4>
                                                            <p>A deep-fried chicken wing coated with a spicy sauce and usually served with a blue cheese dressing.</p>
                                                            <div class="price">
                                                                <h6>75.00</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="tab-item">
                                                            <img src="assets/images/tab-item-koreansoy.jpg" alt="">
                                                            <h4>Korean Soy</h4>
                                                            <p> A deep-fried chicken wing with more light and crispier with a thin, almost paper like skin that is not heavily battered.</p>
                                                            <div class="price">
                                                                <h6>75.00</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="tab-item">
                                                            <img src="assets/images/tab-item-nuttysriracha.jpg" alt="">
                                                            <h4>Nutty Sriracha</h4>
                                                            <p>A deep-fried chicken wing coated with a spicy sauce made from chili paste, garlic, vinegar, sugar, and salt.</p>
                                                            <div class="price">
                                                                <h6>75.00</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="tab-item">
                                                            <img src="assets/images/tab-item-4flavors.jpg" alt="">
                                                            <h4>Barkada Pack</h4>
                                                            <p>4 Savory Chicken Wings</p>
                                                            <div class="price">
                                                                <h6>420.00</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="row">
                                                <div class="right-list">
                                                    <div class="col-lg-12">
                                                        <div class="tab-item">
                                                            <img src="assets/images/tab-item-lemonzest.jpg" alt="">
                                                            <h4>Lemon Zest</h4>
                                                            <p>A deep-fried chicken wing coated with a kosher salt, garlic powder, sweet paprika, and lemon pepper.</p>
                                                            <div class="price">
                                                                <h6>75.00</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="tab-item">
                                                            <img src="assets/images/tab-item-orangeglaze.jpg" alt="">
                                                            <h4>Orange Glazed</h4>
                                                            <p>A deep-fried chicken wing coated with a orange juice and zest, soy sauce, and plenty of garlic.</p>
                                                            <div class="price">
                                                                <h6>75.00</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="tab-item">
                                                            <img src="assets/images/tab-item-mangohabanero.jpg" alt="">
                                                            <h4>Mango Habanero</h4>
                                                            <p>A deep-fried chicken wing coated with a very hot roundish chili pepper that is usually orange when mature.</p>
                                                            <div class="price">
                                                                <h6>75.00</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="tab-item">
                                                            <img src="assets/images/tab-item-5flavors.jpg" alt="">
                                                            <h4>Barkada Pack</h4>
                                                            <p>5 Savory Chicken Wings</p>
                                                            <div class="price">
                                                                <h6>420.00</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </article>  
                                <article id='tabs-2'>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="row">
                                                <div class="left-list">
                                                    <div class="col-lg-12">
                                                        <div class="tab-item">
                                                            <img src="assets/images/tab-item-tonkotsu.jpg" alt="">
                                                            <h4>Tonkotsu</h4>
                                                            <p>A noodle dish born in Japanese made from breaded, deep-fried pork cutlet pork marrow.</p>
                                                            <div class="price">
                                                                <h6>150.00</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="row">
                                                <div class="right-list">
                                                    <div class="col-lg-12">
                                                        <div class="tab-item">
                                                            <img src="assets/images/tab-item-spicytantanmen.jpg" alt="">
                                                            <h4>Spicy Tantamen</h4>
                                                            <p>A noodle dish born in Sichuan Province of China which combines spicy minced beef and sliced Sichuan pickles and has a long history.</p>
                                                            <div class="price">
                                                                <h6>150.00</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </article>  
                                <article id='tabs-3'>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="row">
                                                <div class="left-list">
                                                    <div class="col-lg-12">
                                                        <div class="tab-item">
                                                            <img src="assets/images/tab-item-cheesybacon0.jpg" alt="">
                                                            <h4>Cheesy Bacon</h4>
                                                            <p>Pizza dough, bacon, tomato sauce, black pepper, mozzarella cheese, cheddar cheese.</p>
                                                            <div class="price">
                                                                <h6>180.00</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="tab-item">
                                                            <img src="assets/images/tab-item-pepperonimadness1.jpg" alt="">
                                                            <h4>Pepperoni Madness</h4>
                                                            <p>Pizza dough, cured pork, paprika, garlic, black pepper, crushed red pepper, cayenne pepper, mustard seed, and fennel seed.</p>
                                                            <div class="price">
                                                                <h6>150.00</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="row">
                                                <div class="right-list">
                                                    <div class="col-lg-12">
                                                        <div class="tab-item">
                                                            <img src="assets/images/tab-item-hawaiianoverload2.jpg" alt="">
                                                            <h4>Hawaiian Overload</h4>
                                                            <p>Pizza dough, tomato sauce, cheese, cooked ham, and pineapple.</p>
                                                            <div class="price">
                                                                <h6>150.00</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </article> 
                                <article id='tabs-4'>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="row">
                                                <div class="left-list">
                                                    <div class="col-lg-12">
                                                        <div class="tab-item">
                                                            <img src="assets/images/tab-item-smokedhotdog.jpg" alt="">
                                                            <h4>Smoked Hotdog</h4>
                                                            <p>Hot dog buns, smoked hotdog, melted cheese, tomato sauce.</p>
                                                            <div class="price">
                                                                <h6>55.00</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="tab-item">
                                                            <img src="assets/images/tab-item-bacon&egghotdog.jpg" alt="">
                                                            <h4>Bacon & Egg</h4>
                                                            <p>Hot dog buns, bacon, egg, salt, pepper, oil.</p>
                                                            <div class="price">
                                                                <h6>65.00</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="row">
                                                <div class="right-list">
                                                    <div class="col-lg-12">
                                                        <div class="tab-item">
                                                            <img src="assets/images/tab-item-chickenmushroomhotdog.jpg" alt="">
                                                            <h4>Chicken Mushroom</h4>
                                                            <p>Hot dog buns, chicken fillets, mushrooms,  masala, cheese, salt, pepper, oil, white sauce.</p>
                                                            <div class="price">
                                                                <h6>80.00</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="tab-item">
                                                            <img src="assets/images/tab-item-garlicsteakhotdog.jpg" alt="">
                                                            <h4>Garlic Steak</h4>
                                                            <p>Hot dog buns, butter, fresh parsley,  garlic, reduced-sodium soy sauce, salt, pepper, boneless top sirloin steak.</p>
                                                            <div class="price">
                                                                <h6>90.00</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </article>   
                                  </article>  
                                <article id='tabs-5'>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="row">
                                                <div class="left-list">
                                                    <div class="col-lg-12">
                                                        <div class="tab-item">
                                                            <img src="assets/images/tab-item-solo.jpg" alt="">
                                                            <h4>Solo</h4>
                                                            <p>Tortilla chips, ground beef, olive oil, onion, garlic cloves, refried beans, cheddar,  black beans, salt , taco seasoning, melted cheese.</p>
                                                            <div class="price">
                                                                <h6>75.00</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="tab-item">
                                                            <img src="assets/images/tab-item-barkadapack.jpg" alt="">
                                                            <h4>Barkada Pack</h4>
                                                            <p>Tortilla chips, ground beef, olive oil, onion, garlic cloves, refried beans, cheddar,  black beans, salt , taco seasoning, melted cheese..</p>
                                                            <div class="price">
                                                                <h6>120.00</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="row">
                                                <div class="right-list">
                                                    <div class="col-lg-12">
                                                        <div class="tab-item">
                                                            <img src="assets/images/tab-item-familypack.jpg" alt="">
                                                            <h4>Family Pack</h4>
                                                            <p>Tortilla chips, ground beef, olive oil, onion, garlic cloves, refried beans, cheddar,  black beans, salt , taco seasoning, melted cheese.</p>
                                                            <div class="price">
                                                                <h6>250.00</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="tab-item">
                                                            <img src="assets/images/tab-item-familypack1.jpg" alt="">
                                                            <h4>Family Pack</h4>
                                                            <p>Tortilla chips, ground beef, olive oil, onion, garlic cloves, refried beans, cheddar,  black beans, salt , taco seasoning, melted cheese.</p>
                                                            <div class="price">
                                                                <h6>250.00</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </article> 
                                 <article id='tabs-6'>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="row">
                                                <div class="left-list">
                                                    <div class="col-lg-12">
                                                        <div class="tab-item">
                                                            <img src="assets/images/tab-item-milktea1.jpg" alt="">
                                                            <h4>Nutella Banana</h4>
                                                            <p>Ripe banana, nutella, vanilla, tapioca pearls, tea, ice cub .</p>
                                                            <div class="price">
                                                                <h6>95.00</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="tab-item">
                                                            <img src="assets/images/tab-item-milktea2.jpg" alt="">
                                                            <h4>Nutella Hazelnut</h4>
                                                            <p> Hazelnut creamy, milk cream, honey, tapioca pearls, tea, ice cub.</p>
                                                            <div class="price">
                                                                <h6>95.00</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="tab-item">
                                                            <img src="assets/images/tab-item-milktea3.jpg" alt="">
                                                            <h4>Oreo Chocolate</h4>
                                                            <p>Oreo cookies, milk cream, tapioca pearls, tea, ice cub. </p>
                                                            <div class="price">
                                                                <h6>90.00</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="tab-item">
                                                            <img src="assets/images/tab-item-milktea4.jpg" alt="">
                                                            <h4>White Rabbit Taro</h4>
                                                            <p>White Rabbit Creamy Candies, milk cream, tapioca pearls, tea, ice cub.</p>
                                                            <div class="price">
                                                                <h6>90.00</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="tab-item">
                                                            <img src="assets/images/tab-item-milktea5.jpg" alt="">
                                                            <h4>Crunch Chocolate</h4>
                                                            <p>Crunch chocolate, milk cream, tapioca pearls, tea, ice cub.</p>
                                                            <div class="price">
                                                                <h6>95.00</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="row">
                                                <div class="right-list">
                                                    <div class="col-lg-12">
                                                        <div class="tab-item">
                                                            <img src="assets/images/tab-item-milktea6.jpg" alt="">
                                                            <h4>Kitkat Matcha Cheesecake</h4>
                                                            <p>Kitkat Macha, cheesecake, milk cream, tapioca pearls, tea, ice cub.</p>
                                                            <div class="price">
                                                                <h6>100.00</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="tab-item">
                                                            <img src="assets/images/tab-item-milktea7.jpg" alt="">
                                                            <h4>Nutella Hazelnut</h4>
                                                            <p>Hazelnut creamy, milk cream, honey, tapioca pearls, tea, ice cub.</p>
                                                            <div class="price">
                                                                <h6>155.00</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="tab-item">
                                                            <img src="assets/images/tab-item-milktea8.jpg" alt="">
                                                            <h4>Strawberry Yakult</h4>
                                                            <p>Strawberry sauce, yaculy, tapioca pearls, tea, ice cub.</p>
                                                            <div class="price">
                                                                <h6>75.00</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="tab-item">
                                                            <img src="assets/images/tab-item-milktea9.jpg" alt="">
                                                            <h4>Dark Choco Cheesecake</h4>
                                                            <p>Dark Chocolate, cheesecake, milk cream, tapioca pearls, tea, ice cub.</p>
                                                            <div class="price">
                                                                <h6>140.00</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="tab-item">
                                                            <img src="assets/images/tab-item-milktea10.jpg" alt="">
                                                            <h4>Red Velvet Cheesecake</h4>
                                                            <p>Red Velvet, cheesecake, milk cream, tapioca pearls, tea, ice cub.</p>
                                                            <div class="price">
                                                                <h6>140.00</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </article>  

                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Chefs Area Ends ***** --> 
    
    <!-- ***** Footer Start ***** -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-xs-12">
                    <div class="right-text-content">
                            <ul class="social-icons">
                                <li><a href="https://web.facebook.com/profile.php?id=100063927280174"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="https://twitter.com/ameliaflorvi"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="https://instagram.com/ameliaflorvi?igshid=YmMyMTA2M2Y="><i class="fa fa-instagram"></i></a></li>
                            </ul>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="logo">
                        <a href="index.html"><img src="assets/images/hangryboss-logo.png" alt="" width="100px" height="100px" ></a>
                    </div>
                </div>
                <div class="col-lg-4 col-xs-12">
                    <div class="left-text-content">
                       <p>© 2022 Hangry Boss
                        <br>All Right Reserved.
                        <br>Created by:&nbsp&nbsp&nbsp&nbsp&nbsp 
                        <br>Edilyn De Guzman.</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="assets/js/jquery-2.1.0.min.js"></script>

    <!-- Bootstrap -->
    <script src="assets/js/popper.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- Plugins -->
    <script src="assets/js/owl-carousel.js"></script>
    <script src="assets/js/accordions.js"></script>
    <script src="assets/js/datepicker.js"></script>
    <script src="assets/js/scrollreveal.min.js"></script>
    <script src="assets/js/waypoints.min.js"></script>
    <script src="assets/js/jquery.counterup.min.js"></script>
    <script src="assets/js/imgfix.min.js"></script> 
    <script src="assets/js/slick.js"></script> 
    <script src="assets/js/lightbox.js"></script> 
    <script src="assets/js/isotope.js"></script> 
    
    <!-- Global Init -->
    <script src="assets/js/custom.js"></script>
    <script>

        $(function() {
            var selectedClass = "";
            $("p").click(function(){
            selectedClass = $(this).attr("data-rel");
            $("#portfolio").fadeTo(50, 0.1);
                $("#portfolio div").not("."+selectedClass).fadeOut();
            setTimeout(function() {
              $("."+selectedClass).fadeIn();
              $("#portfolio").fadeTo(50, 1);
            }, 500);
                
            });
        });

        

    </script>
  </body>
</html>