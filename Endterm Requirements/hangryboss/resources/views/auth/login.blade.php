<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Dancing+Script:wght@400;500;600;700&display=swap" rel="stylesheet">

    <title>Hangry Boss Content Management System</title>
<!--
    
TemplateMo 558 Klassy Cafe

https://templatemo.com/tm-558-klassy-cafe

-->
    <!-- Additional CSS Files -->
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">

    <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.css">

    <link rel="stylesheet" href="assets/css/templatemo-klassy-cafe.css">

     <link rel="stylesheet" href="assets/css/hangryboss.css">

    <link rel="stylesheet" href="assets/css/owl-carousel.css">

    <link rel="stylesheet" href="assets/css/lightbox.css">

    </head>
    <style>
        .logo{
        margin-left: 5px;
        }
        footer{
            margin-top: 0;
        }

        footer .right-text-content p {
        color: #fff;
        font-size: 12px;
        margin-right: 22px;
        text-transform: uppercase;
        }

        footer .left-text-content p {
        margin-top: 5px;
        color: #fff;
        font-size: 17px;
        text-align: right;
        }

    </style>
    
    <body>
    
    <!-- ***** Preloader Start ***** -->

    <!-- ***** Preloader End ***** -->
    
    
    <!-- ***** Header Area Start ***** -->
    <header class="header-area header-sticky">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <nav class="main-nav">
                        <!-- ***** Logo Start ***** -->
                        <a href="index.html" class="logo">
                            <img src="assets/images/hangryboss-logo.png" align="Hangry Boss html template" width="120px" height="120px">
                        </a>
                        <!-- ***** Logo End ***** -->
                        <!-- ***** Menu Start ***** -->
                        <ul class="nav">
                            <li class="scroll-to-section"><a href="#top" class="font-weight-bold">HOME</a></li>
                            <li class="scroll-to-section"><a href="#about" class="font-weight-bold">ABOUT</a></li>
                            <li class="scroll-to-section"><a href="#menu" class="font-weight-bold">MENU</a></li>
                            <li class="scroll-to-section"><a href="#chefs" class="font-weight-bold">CHEFS</a></li> 
                            <li class="scroll-to-section"><a href="#reservation" class="font-weight-bold">CONTACT US</a></li> 

                        
                            <li>
                            @if (Route::has('login'))
                                <div class=" hidden fixed top-0 right-0 px-6 py-4 sm:block ">
                            @auth
                                 <li><x-app-layout>
    
                                 </x-app-layout>
                            </li>
                            @else
                                 <li><a href="{{ route('login') }}" class="text-sm text-gray-700 dark:text-gray-500 underline font-weight-bold">LOGIN</a></li>

                            @if (Route::has('register'))
                            <li><a href="{{ route('register') }}" class="ml-1 text-sm text-gray-700 dark:text-gray-500 underline font-weight-bold">REGISTER</a></li>
                            @endif
                            @endauth
                            </div>
                            @endif   


                            </li>
                        
                        <!-- ***** Menu End ***** -->
                    </nav>
                </div>
            </div>
        </div>
    </header>
<x-guest-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
            <x-jet-authentication-card-logo />
        </x-slot>

        <x-jet-validation-errors class="mb-4" />

        @if (session('status'))
            <div class="mb-4 font-medium text-sm text-green-600">
                {{ session('status') }}
            </div>
        @endif

        <form method="POST" action="{{ route('login') }}">
            @csrf

            <div>
                <x-jet-label for="email" value="{{ __('Email') }}" />
                <x-jet-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required autofocus />
            </div>

            <div class="mt-4">
                <x-jet-label for="password" value="{{ __('Password') }}" />
                <x-jet-input id="password" class="block mt-1 w-full" type="password" name="password" required autocomplete="current-password" />
            </div>

            <div class="block mt-4">
                <label for="remember_me" class="flex items-center">
                    <x-jet-checkbox id="remember_me" name="remember" />
                    <span class="ml-2 text-sm text-gray-600">{{ __('Remember me') }}</span>
                </label>
            </div>

            <div class="flex items-center justify-end mt-4">
                @if (Route::has('password.request'))
                    <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('password.request') }}">
                        {{ __('Forgot your password?') }}
                    </a>
                @endif

                <x-jet-button class="ml-4">
                    {{ __('Log in') }}
                </x-jet-button>
            </div>
        </form>
    </x-jet-authentication-card>
</x-guest-layout>
 <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-xs-12 ml-5 mt-0">
                    <div class="right-text-content">
                            <ul class="social-icons">
                                <li><a href="https://web.facebook.com/profile.php?id=100063927280174"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="https://twitter.com/ameliaflorvi"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="https://instagram.com/ameliaflorvi?igshid=YmMyMTA2M2Y="><i class="fa fa-instagram"></i></a></li>
                            </ul>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="logo">
                        <a href="index.html"><img src="assets/images/hangryboss-logo.png" alt="" width="100px" height="100px" ></a>
                    </div>
                </div>
                
                    <div class="left-text-content">
                        <p>© 2022 Hangry Boss
                        <br>All Right Reserved.
                        <br>Created by:&nbsp&nbsp&nbsp&nbsp&nbsp 
                        <br>Edilyn De Guzman.</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</ul>
</nav>
</div>
</div>
</div>
</header>
</body>
</html>